//
//  TextModifier.swift
//  
//
//  Created by Ilham Prabawa on 12/06/23.
//

import SwiftUI

struct Title: ViewModifier {
  
  let size: CGFloat
  
  init(size: CGFloat = 32) {
    self.size = size
  }
  
  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.dmSansFont(style: .title(size: size))))
      .foregroundColor(Color.darkTextColor)
  }
}

struct Medium: ViewModifier {
  
  let size: CGFloat
  
  init(size: CGFloat = 32) {
    self.size = size
  }
  
  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.dmSansFont(style: .body(size: size))))
      .foregroundColor(Color.darkTextColor)
  }
  
}

struct Caption: ViewModifier {
  
  let size: CGFloat
  
  init(size: CGFloat = 32) {
    self.size = size
  }
  
  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.dmSansFont(style: .caption(size: size))))
      .foregroundColor(Color.darkTextColor)
  }
  
}

struct SemiBoldLexend: ViewModifier {

  let size: CGFloat

  init(size: CGFloat = 32) {
    self.size = size
  }

  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.lexendFont(style: .title(size: size))))
      .foregroundColor(Color.darkTextColor)
  }
}

struct RegularLexend: ViewModifier {

  let size: CGFloat

  init(size: CGFloat = 32) {
    self.size = size
  }

  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.lexendFont(style: .body(size: size))))
      .foregroundColor(Color.darkTextColor)
  }

}

struct LightLexend: ViewModifier {

  let size: CGFloat

  init(size: CGFloat = 32) {
    self.size = size
  }

  func body(content: Content) -> some View {
    content
      .font(Font(UIFont.lexendFont(style: .caption(size: size))))
      .foregroundColor(Color.darkTextColor)
  }

}

