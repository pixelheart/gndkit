//
//  StatusView.swift
//  
//
//  Created by Ilham Prabawa on 16/05/23.
//

import SwiftUI

public struct StatusView: View {
  
  @Binding var isOnline: Bool
  var tapAction: () -> Void
  
  public init(isOnline: Binding<Bool>,
              tapAction: @escaping () -> Void) {
    self._isOnline = isOnline
    self.tapAction = tapAction
  }
  
  public var body: some View {
    ZStack(alignment: isOnline ? .leading : .trailing) {
      Capsule()
        .fill(isOnline ? Color.toggleActive : Color.gray)
        .frame(width: 90, height: 25)
        .animation(.default, value: isOnline)
        .overlay(
          Circle()
            .fill(isOnline ? Color.white : Color.black.opacity(0.3))
            .frame(height: 19)
            .position(x: isOnline ? (90 - 13) : 13, y: 12.5)
            .animation(
              Animation.spring(
                response: 0.2,
                dampingFraction: 0.6,
                blendDuration: 1
              ),
              value: isOnline
            )
        )
        .onTapGesture {
          tapAction()
//          isOnline.toggle()
        }
      
      Text(isOnline ? "Online": "Offline")
        .font(Font(UIFont.dmSansFont(style: .title(size: 16))))
        .foregroundColor(Color.white)
        .padding(.horizontal, 8)
    }
    .animation(
      Animation.spring(
        response: 0.2,
        dampingFraction: 0.5,
        blendDuration: 1
      ),
      value: isOnline
    )
    
  }
}

struct StatusView_PreviewView: PreviewProvider {
  static var previews: some View {
    StatusView(isOnline: .constant(false), tapAction: {})
  }
}
