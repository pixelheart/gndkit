//
//  ToggleView.swift
//
//
//  Created by Ilham Prabawa on 26/09/24.
//

import SwiftUI

public struct ToggleView: View {

  @Binding var isActive: Bool
  var tapAction: () -> Void

  public init(
    isActive: Binding<Bool>,
    tapAction: @escaping () -> Void
  ) {
    self._isActive = isActive
    self.tapAction = tapAction
  }

  public var body: some View {
    ZStack(alignment: isActive ? .leading : .trailing) {
      Capsule()
        .fill(isActive ? Color.green80 : Color.darkGray300)
        .frame(width: 38, height: 20)
        .animation(.default, value: isActive)
        .overlay(
          Circle()
            .fill(Color.white)
            .frame(height: 14)
            .position(x: isActive ? (38 - 10) : 10, y: 10)
            .animation(
              Animation.spring(
                response: 0.2,
                dampingFraction: 0.6,
                blendDuration: 1
              ),
              value: isActive
            )
            .overlay(
              Circle()
                .stroke(
                  isActive ? Color.successColor : Color.darkGray400,
                  lineWidth: 1
                )
                .frame(height: 16)
                .position(x: isActive ? (38 - 10) : 10, y: 10)
            )
        )
        .onTapGesture {
          tapAction()
          isActive.toggle()
        }

    }
    .animation(
      Animation.spring(
        response: 0.2,
        dampingFraction: 0.5,
        blendDuration: 1
      ),
      value: isActive
    )

  }
}

#Preview {
  ToggleView(
    isActive: .constant(false),
    tapAction: {}
  )
}
