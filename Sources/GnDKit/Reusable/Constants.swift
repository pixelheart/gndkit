//
//  Constants.swift
//  
//
//  Created by Ilham Prabawa on 04/04/23.
//

import Foundation
import UIKit

public let screen = UIScreen.main.bounds
public let deviceType: String = UIDevice.current.model
public let deviceID: String = UIDevice.current.identifierForVendor!.uuidString
