//
//  Border.swift
//
//
//  Created by Ilham Prabawa on 26/09/24.
//

import Foundation
import SwiftUI

public struct Border {

  public let width: CGFloat
  public let height: CGFloat
  public let color: Color
  public let lineWidth: CGFloat

  public init(
    width: CGFloat,
    height: CGFloat,
    color: Color,
    lineWidth: CGFloat
  ) {
    self.width = width
    self.height = height
    self.color = color
    self.lineWidth = lineWidth
  }

}
