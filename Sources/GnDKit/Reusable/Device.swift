//
//  File.swift
//  
//
//  Created by muhammad yusuf on 25/08/23.
//

import UIKit

extension UIDevice {
    var hasTopNotch: Bool {
      if #available(iOS 11.0, tvOS 11.0, *) {
        return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
      }
      return false
    }
}
