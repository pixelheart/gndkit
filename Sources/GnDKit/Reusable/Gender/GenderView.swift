//
//  GenderView.swift
//  
//
//  Created by Ilham Prabawa on 14/06/23.
//

import SwiftUI

public struct GenderView: View {
  
  let imageName: String
  let title: String
  let gender: Gender
  let action: () -> Void
  
  @Binding var selectedGender: Gender
  
  public init(imageName: String,
              title: String,
              gender: Gender,
              selectedGender: Binding<Gender>,
              action: @escaping () -> Void) {
    
    self.imageName = imageName
    self.title = title
    self.gender = gender
    self.action = action
    self._selectedGender = selectedGender
  }
  
  public var body: some View {
    HStack {
      Image(imageName, bundle: .module)
        .renderingMode(.template)
        .foregroundColor(selectedGender == gender
                         ? Color(UIColor.buttonActiveColor)
                         : Color(UIColor.darkTextColor))
      Text(NSLocalizedString(title, comment: ""))
        .font(Font(UIFont.dmSansFont(style: .body(size: 12))))
        .foregroundColor(selectedGender == gender
                         ? Color(UIColor.buttonActiveColor)
                         : Color(UIColor.darkTextColor))
    }
    .frame(height: 40)
    .padding(.vertical, 3)
    .padding(.horizontal, 12)
    .background(selectedGender == gender
                ? Color.blue.opacity(0.3)
                : Color.white)
    .clipShape(RoundedRectangle(cornerRadius: 40/2+12))
    .overlay(
      RoundedRectangle(cornerRadius: 40/2+12).stroke(style: .init(lineWidth: 1))
        .foregroundColor(Color.gray.opacity(0.3))
    )
    .onTapGesture {
      action()
    }
  }
}


struct GenderView_Previews: PreviewProvider {
  static var previews: some View {
    GenderView(
      imageName: "",
      title: "",
      gender: .men,
      selectedGender: .constant(.men),
      action: {}
    )
  }
}
