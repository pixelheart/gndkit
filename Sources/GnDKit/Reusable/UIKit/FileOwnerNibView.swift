//
//  FileOwnerNibView.swift
//  
//
//  Created by Ilham Prabawa on 14/04/23.
//

import UIKit

open class FileOwnerNibView: UIView {
  
  public var contentView: UIView?
  public var bundleOrNil: Bundle?
  public var hierarchyNotReady = true
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    onInit()
  }
  
  public required init?(coder: NSCoder) {
    super.init(coder: coder)
    onInit()
  }
  
  open func onInit() {
    setupBundle()
    let nib = UINib(nibName: nibName, bundle: self.bundleOrNil)
    guard let contentView = nib.instantiate(withOwner: self).first as? UIView else {
      return
    }
    self.contentView = contentView
    setupView()
  }
  
  open func setupBundle() {
    self.bundleOrNil = .module
    fatalError("Must define bundle by overriding setupBundle()")
  }
  
  open func setupView() {
    guard let contentView = self.contentView else { return }
    contentView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(contentView)
    NSLayoutConstraint.activate([
      contentView.topAnchor.constraint(equalTo: topAnchor),
      contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
      contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
      contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }
  
  open var nibName: String { "\(Self.self)" }
  
}
