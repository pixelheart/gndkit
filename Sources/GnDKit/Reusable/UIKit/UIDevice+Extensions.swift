//
//  UIDevice+Extensions.swift
//
//
//  Created by Ilham Prabawa on 20/11/23.
//

import Foundation
import UIKit
import AudioToolbox

extension UIDevice {

  public static func vibrate() {
    AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
      AudioServicesDisposeSystemSoundID(SystemSoundID(kSystemSoundID_Vibrate))
    }
  }

}
