//
//  MessangerViewController.swift
//  Testing
//
//  Created by Ilham Prabawa on 10/04/23.
//

import UIKit
import Combine

public class TabCell: UICollectionViewCell {
  
  public static let identifier: String = String(describing: TabCell.self)

  let titleLabel: UILabel = {
    let label = UILabel()
    label.text = "mantab"
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = .white
    
    contentView.addSubview(titleLabel)
    NSLayoutConstraint.activate([
      titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
      titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

public class TabbedPageViewController: NiblessViewController {
  
  var pageViewController: PageViewController
  
  //MARK: - View
  let indicatorView = UIView()
  
  var heightAnchor: NSLayoutConstraint!
  
  lazy var tabCollectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.register(TabCell.self, forCellWithReuseIdentifier: TabCell.identifier)
    cv.dataSource = self
    cv.delegate = self
    cv.translatesAutoresizingMaskIntoConstraints = false
    cv.backgroundColor = .clear
    return cv
  }()
  
  
  //MARK: - Subscription
  private var subscriptions = Set<AnyCancellable>()
  
  //MARK: - Properties
  private var spacing: CGFloat = 0
  public var viewControllers: [UIViewController] = [] {
    didSet {
      setupView()
      tabCollectionView.reloadData()
    }
  }
  
  public override init() {
    self.pageViewController = PageViewController(
      transitionStyle: .scroll,
      navigationOrientation: .horizontal
    )
    
    super.init()
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    
  }
  
  private func setupView() {
    setupTabCollection()
    setupIndicator()
    setupPage()
    observePage()
  }
  
  //MARK: - Private Function
  private func setupTabCollection() {
    view.addSubview(tabCollectionView)
    heightAnchor = tabCollectionView.heightAnchor.constraint(equalToConstant: 100)
    NSLayoutConstraint.activate([
      tabCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
      tabCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      tabCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      heightAnchor
    ])
    
  }
  
  private func setupIndicator() {
    let width = view.bounds.width / CGFloat(viewControllers.count)
    indicatorView.layer.cornerRadius = 2
    indicatorView.translatesAutoresizingMaskIntoConstraints = false
    indicatorView.backgroundColor = UIColor.gray
    indicatorView.frame = CGRect(x: 0, y: heightAnchor.constant - 5, width: width, height: 5)
    tabCollectionView.addSubview(indicatorView)
  }
  
  private func setupPage() {
    pageViewController.controllers = viewControllers
    addChild(pageViewController)
    pageViewController.willMove(toParent: self)
    view.addSubview(pageViewController.view)
    pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      pageViewController.view.topAnchor.constraint(equalTo: tabCollectionView.bottomAnchor),
      pageViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      pageViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      pageViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ])
  }
  
  private func updatePage(with index: Int) {
    let width = Int(view.bounds.width) / viewControllers.count
    
    UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve) {
      self.indicatorView.frame.origin.x = CGFloat(width * index) + self.spacing
    }
    
  }
  
  private func observePage() {
    pageViewController.slideTo
      .receive(on: RunLoop.main)
      .sink { [weak self] indexPath in
        guard let self = self else { return }
        self.updatePage(with: indexPath.item)
      }.store(in: &subscriptions)
  }
  
}

extension TabbedPageViewController: UICollectionViewDataSource {
  
  public func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    
    return viewControllers.count
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabCell.identifier, for: indexPath) as! TabCell
    return cell
  }
  
}

extension TabbedPageViewController: UICollectionViewDelegate {
  
  public func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    
  }
  
}

extension TabbedPageViewController: UICollectionViewDelegateFlowLayout {
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    let width = Int(collectionView.frame.width)
    return .init(width: width / viewControllers.count, height: 100)
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    
    return spacing
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt section: Int
  ) -> CGFloat {
    return 0
  }
  
}
