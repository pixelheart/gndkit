//
//  UIViewController+NavigationBar.swift
//  
//
//  Created by Ilham Prabawa on 05/05/23.
//

import UIKit

extension UIViewController {
  
  public func backImage(_ named: String) -> UIImage{
    return UIImage(named: named,
                   in: .module,
                   compatibleWith: .none)!.withRenderingMode(.alwaysOriginal)
  }
  
  public func titleLabel(_ value: String) -> UILabel {
    let titleLabel = UILabel()
    titleLabel.text = value
    titleLabel.font = UIFont.dmSansFont(style: .title(size: 16))
    titleLabel.textColor = UIColor.darkTextColor
    return titleLabel
  }
  
  public func standardNavBar(title: String = "") {
    navigationItem.leftBarButtonItems = [
      UIBarButtonItem(
        image: backImage("ic_arrow_back"),
        style: .plain,
        target: self,
        action: #selector(didBack)
      ),
      UIBarButtonItem(customView: titleLabel(title))
    ]
  }
  
  @objc
  open func didBack() {
    navigationController?.popViewController(animated: true)
  }
  
}
