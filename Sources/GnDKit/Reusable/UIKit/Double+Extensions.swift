//
//  Double+Extensions.swift
//  
//
//  Created by Ilham Prabawa on 10/08/23.
//

import Foundation

extension Double {
  
  public func timeString() -> String {
    let _ = Int(self) / 3600
    let minutes = Int(self) / 60 % 60
    let seconds = Int(self) % 60
    return String(format: "%02i:%02i", minutes, seconds)
  }

  public func convertFromEpoch(with format: String) -> String {
    let date = Date(timeIntervalSince1970: TimeInterval(self) / 1000)
    let formatter = DateFormatter()
    formatter.timeZone = .current
    formatter.dateFormat = format
    return formatter.string(from: date)
  }

  public func toDate() -> Date {
    Date(timeIntervalSince1970: TimeInterval(self) / 1000)
  }

  public func epochToDate() -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .medium

    let date = Date(timeIntervalSince1970: self)
    let formattedDate = dateFormatter.string(from: date)
    return dateFormatter.date(from: formattedDate)!
  }

}
