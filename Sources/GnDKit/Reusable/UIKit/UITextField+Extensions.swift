//
//  UITextField+Extensions.swift
//  
//
//  Created by Ilham Prabawa on 15/04/23.
//

import UIKit
import Combine

extension UITextField {
  
  public func textStyle() {
    borderStyle = .none
    layer.backgroundColor = UIColor.textFieldBackgroundColor.cgColor
    layer.cornerRadius = 8
    leftView = UIView(frame: .init(x: 0, y: 0, width: 12, height: 12))
    leftViewMode = .always
  }
  
  public func passwordStyle() {
    borderStyle = .none
    layer.backgroundColor = UIColor.textFieldBackgroundColor.cgColor
    layer.cornerRadius = 8
    leftView = UIView(frame: .init(x: 0, y: 0, width: 12, height: 12))
    leftViewMode = .always
    rightView = UIView(frame: .init(x: 0, y: 0, width: 46, height: 46))
    rightViewMode = .always
  }
  
  public func addBorder(color: UIColor) {
    layer.borderColor = color.cgColor
    layer.borderWidth = 1
    layer.cornerRadius = 8
    layer.masksToBounds = true
  }
  
  public func removeBorder() {
    layer.borderWidth = 0
  }
}


extension UITextField {

  public var textPublisher: AnyPublisher<String?, Never> {
    NotificationCenter.default.publisher(
      for: UITextField.textDidChangeNotification,
      object: self
    )
    .map { notification in
      guard let textField = notification.object as? UITextField else {
        return nil
      }

      return textField.text

    }.eraseToAnyPublisher()
  }

}
