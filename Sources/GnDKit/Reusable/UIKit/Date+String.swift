//
//  Date+String.swift
//  
//
//  Created by Ilham Prabawa on 28/04/23.
//

import Foundation

public extension Date {
  
  func stringFormat() -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = .init(identifier: "id")
    dateFormatter.dateFormat = "EEEE, MMMM d, yyyy HH:mm"
    return dateFormatter.string(from: self)
  }
  
  func formatted(with format: String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
  
  func findMinutesDiff(with compared: Date) -> TimeInterval {
    let timeformatter = DateFormatter()
    timeformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    let interval = compared.timeIntervalSince(self)
//    let hours = interval / 3600;
//    let minutes = interval.truncatingRemainder(dividingBy: 3600) / 60
//    NLog("timeinterval", interval)
//    NLog("hours", Int(hours))
//    NLog("minutes", Int(minutes))
    return interval
  }

  func nextDay(by day: Int) -> Date? {
    let nextDay = Calendar.current.date(byAdding: .day, value: day, to: self)!
    let component = Calendar.current.dateComponents(
      [
        .year,
        .month,
        .day,
        .hour,
        .minute
      ],
      from: nextDay
    )
    return Calendar.current.date(from: component)
  }

  func addingHour(by count: Int) -> Date? {
    let hour = Calendar.current.date(byAdding: .hour, value: count, to: self)!
    let component = Calendar.current.dateComponents(
      [
        .year,
        .month,
        .day,
        .hour,
        .minute
      ],
      from: hour
    )
    return Calendar.current.date(from: component)
  }

}
