//
//  UIView+Extensions.swift
//  Ilham Hadi Prabawa
//
//  Created by Ilham Hadi Prabawa on 06/01/22.
//  Copyright © 2022 Xamben. All rights reserved.
//

import UIKit
import Foundation

public extension UIView {
  
  class func fromNib<T: UIView>() -> T {
    
    return Bundle(for: T.self).loadNibNamed(String(describing: T.self),
                                            owner: nil,
                                            options: nil)![0] as! T
    
  }
  
  func addShadow(cornerRadius: CGFloat,
                 shadowRadius: CGFloat,
                 shadowOffset: CGSize,
                 color: UIColor){
    
    layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    layer.shadowRadius = shadowRadius
    layer.shadowOffset = shadowOffset
    layer.shadowOpacity = 1
    layer.shadowColor = color.cgColor
  }
  
  func equalToSuperView(of contentView: UIView) {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      contentView.topAnchor.constraint(equalTo: topAnchor),
      contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
      contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
      contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }
  
  func moveViewWithKeyboard(notification: NSNotification,
                            viewBottomConstraint: NSLayoutConstraint,
                            keyboardWillShow: Bool) {
    // Keyboard's size
    guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    
    let keyboardHeight = keyboardSize.height
    
    // Keyboard's animation duration
    let keyboardDuration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
    
    // Keyboard's animation curve
    let keyboardCurve = UIView.AnimationCurve(rawValue: notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! Int)!
    
    if keyboardWillShow {
      let bottomConstant: CGFloat = 100
      viewBottomConstraint.constant = keyboardHeight + bottomConstant
    }else {
      viewBottomConstraint.constant = 130
    }
    
    let animator = UIViewPropertyAnimator(duration: keyboardDuration, curve: keyboardCurve) { [weak self] in
      self?.layoutIfNeeded()
    }
    
    animator.startAnimation()
    
  }
  
}

public extension UIView {

  func xibSetup(nibName : String) {
    var containerView = UIView()
    containerView = loadViewFromNib(nibName: nibName)
    containerView.frame = bounds
    containerView.autoresizingMask = [ .flexibleWidth, .flexibleHeight]
    addSubview(containerView)
  }
  
  func loadViewFromNib(nibName : String) -> UIView {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: nibName, bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }

  func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(
      roundedRect: bounds,
      byRoundingCorners: corners,
      cornerRadii: CGSize(
        width: radius,
        height: radius
      )
    )
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
  }

  func roundCorners(_ corners: UIRectCorner = .allCorners, value: CGFloat) {
    guard corners != .allCorners else {
      layer.cornerRadius = value
      return
    }
    
    guard #available(iOS 11.0, *) else {
      let path = UIBezierPath(roundedRect: bounds,
                              byRoundingCorners: corners,
                              cornerRadii: CGSize(width: value, height: value))
      let maskLayer = CAShapeLayer()
      maskLayer.frame = bounds
      maskLayer.path = path.cgPath
      layer.mask = maskLayer
      
      return
    }
    
    layer.cornerRadius = value
    layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
  }
  
  func addShadow(cornerRadius: CGFloat) {
    layer.shadowOffset = CGSize(width:0, height:0)
    layer.cornerRadius = cornerRadius
    layer.borderColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.11).cgColor
    //layer.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
    layer.borderWidth = 1
    layer.shadowRadius = 4
    layer.shadowOpacity = 0.11
    layer.masksToBounds = false
  }
  
}
