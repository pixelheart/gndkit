//
//  String+Extensions.swift
//  
//
//  Created by Ilham Prabawa on 05/04/23.
//

import Foundation
import UIKit

public extension String{

  func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin,
                                        attributes: [NSAttributedString.Key.font: font], context: nil)

    return ceil(boundingBox.height)
  }

  func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin,
                                        attributes: [NSAttributedString.Key.font: font], context: nil)

    return ceil(boundingBox.width)
  }

  func isValidEmail() -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
    return emailPredicate.evaluate(with: self)
  }

  func toDate() -> Date? {
    let formatter = DateFormatter()
    formatter.locale = Locale.current
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    return formatter.date(from: self)
  }

  func fileName() -> String {
    return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
  }

  func fileExtension() -> String {
    return URL(fileURLWithPath: self).pathExtension
  }

  func truncate(_ length: Int) -> String {
    self.count > length ? self.prefix(length) + "..." : self
  }

}

