//
//  UITextView+Extensions.swift
//  
//
//  Created by Ilham Prabawa on 05/10/23.
//

import Foundation
import UIKit
import Combine

extension UITextView {

  public var textPublisher: AnyPublisher<String?, Never> {
    NotificationCenter.default.publisher(
      for: UITextView.textDidChangeNotification,
      object: self
    )
    .map { notification in
      guard let textView = notification.object as? UITextView else {
        return nil
      }

      return textView.text
    }.eraseToAnyPublisher()
  }

}
