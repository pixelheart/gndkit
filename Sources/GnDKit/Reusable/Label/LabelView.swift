//
//  LabelView.swift
//  
//
//  Created by Ilham Prabawa on 15/05/23.
//

import SwiftUI

public struct LabelView: View {
  
  private let title: String
  private let textColor: Color
  
  public init(title: String, textColor: Color) {
    self.title = title
    self.textColor = textColor
  }
  
  public var body: some View {
    Text(title)
      .font(Font(UIFont.dmSansFont(style: .title(size: 12))))
      .foregroundColor(textColor)
      .padding(.all, 4)
      .background(textColor.opacity(0.1))
      .cornerRadius(4)
  }
}
