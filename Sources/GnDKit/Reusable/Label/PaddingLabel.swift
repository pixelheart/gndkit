//
//  PaddingLabel.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import Foundation
import UIKit

public class PaddingLabel: UILabel {

  public var top: CGFloat = 8
  public var left: CGFloat = 8
  public var right: CGFloat = 8
  public var bottom: CGFloat = 8

  public init(
    top: CGFloat,
    left: CGFloat,
    right: CGFloat,
    bottom: CGFloat
  ) {
    self.top = top
    self.left = left
    self.right = right
    self.bottom = bottom

    super.init(frame: .zero)
  }

  public init(
    vertical: CGFloat,
    horizontal: CGFloat
  ) {
    self.top = vertical
    self.bottom = vertical
    self.left = horizontal
    self.right = horizontal

    super.init(frame: .zero)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  public override func drawText(in rect: CGRect) {
    let insets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    super.drawText(in: rect.inset(by: insets))
  }

  public override var intrinsicContentSize: CGSize {
    let size = super.intrinsicContentSize
    return CGSize(
      width: size.width + left + right,
      height: size.height + top + bottom
    )
  }

  public override var bounds: CGRect {
    didSet {
      preferredMaxLayoutWidth = bounds.width - (left + right)
    }
  }

}
