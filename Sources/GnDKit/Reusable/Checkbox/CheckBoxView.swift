//
//  CheckBoxView.swift
//  
//
//  Created by Ilham Prabawa on 13/06/23.
//

import SwiftUI

public final class CheckboxModel: Identifiable {
  public var id: Int
  let value: String
  var isChecked: Bool
  
  public init(id: Int,
              value: String,
              isChecked: Bool = false) {
    
    self.id = id
    self.value = value
    self.isChecked = isChecked
  }
}

public struct CheckBoxView: View {
  
  @Binding var model: [CheckboxModel]
  
  let columns = [
    GridItem(.flexible()),
    GridItem(.flexible()),
  ]
  
  public init(model: Binding<[CheckboxModel]>) {
    self._model = model
  }
  
  public var body: some View {
    LazyVGrid(columns: columns, alignment: .leading) {
      ForEach($model, id: \.id) { item in
        HStack {
          RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1)
            .frame(width: 25, height: 25)
            .overlay(
              showChecked(item.isChecked)
            )
          Text(item.wrappedValue.value)
            .bodyStyle()
        }
        .onTapGesture {
          item.isChecked.wrappedValue.toggle()
        }
      }
    }
  }
  
  @ViewBuilder
  func showChecked(_ checked: Binding<Bool>) -> some View {
    if checked.wrappedValue {
      Image(systemName: "checkmark")
        .resizable()
        .frame(width: 13, height: 10)
    }else {
      EmptyView()
    }
  }
}

struct CheckBoxView_Previews: PreviewProvider {
  static var previews: some View {
    CheckBoxView(model: .constant(
      [
        CheckboxModel(id: 1, value: "Pidana", isChecked: true),
        CheckboxModel(id: 2, value: "Perdata"),
        CheckboxModel(id: 3, value: "Ketenagakerjaan"),
        CheckboxModel(id: 4, value: "Perkawinan & Perceraian"),
        CheckboxModel(id: 5, value: "Pertanahan"),
        CheckboxModel(id: 6, value: "Perpajakan"),
        CheckboxModel(id: 7, value: "Kepailitan dan PKPU")
      ]
    ))
  }
}
