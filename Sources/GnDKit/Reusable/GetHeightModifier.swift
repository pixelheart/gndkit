//
//  GetHeightModifier.swift
//  
//
//  Created by Ilham Prabawa on 29/05/23.
//

import SwiftUI

public struct GetHeightModifier: ViewModifier {
  @Binding var height: CGFloat
  
  public init(height: Binding<CGFloat>) {
    self._height = height
  }
  
  public func body(content: Content) -> some View {
    content.background(
      GeometryReader { geometry -> Color in
        DispatchQueue.main.async {
          height = geometry.size.height
        }
        
        return Color.clear
      }
    )
  }
}

