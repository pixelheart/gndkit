//
//  ScrollViewKit.swift
//  
//
//  Created by Ilham Prabawa on 29/05/23.
//

import SwiftUI

public struct ScrollViewKit<Content>: UIViewRepresentable where Content : View {

  let content: () -> Content
  @Binding var scrollEnabled: Bool
  
  public init(scrollEnabled: Binding<Bool>,
              @ViewBuilder content: @escaping () -> Content) {
    
    self._scrollEnabled = scrollEnabled
    self.content = content
  }
  
  public func makeUIView(context: Context) -> UIScrollView {
    let hosting = UIHostingController(rootView: content())
    let size = hosting.sizeThatFits(in: CGSize(width: screen.width, height: CGFloat.greatestFiniteMagnitude))
    hosting.view.frame = CGRect(x: 0, y: 0, width: screen.width, height: size.height)
    
    let view = UIScrollView()
    view.alwaysBounceVertical = true
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false
    view.addSubview(hosting.view)
    view.contentSize = CGSize(width: screen.width, height: size.height)
    return view
  }
  
  public func updateUIView(_ uiView: UIScrollView, context: Context) {
    
    uiView.isScrollEnabled = scrollEnabled
  }
  
  
}
