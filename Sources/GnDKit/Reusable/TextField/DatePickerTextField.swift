//
//  DatePickerTextField.swift
//  
//
//  Created by Ilham Prabawa on 20/04/23.
//

import SwiftUI
import UIKit

public final class DatePickerTextField: UITextField {
  @Binding var date: Date?
  private let datePicker = UIDatePicker()
  
  public init(date: Binding<Date?>, frame: CGRect) {
    self._date = date
    super.init(frame: frame)
    inputView = datePicker
    datePicker.addTarget(self, action: #selector(datePickerDidSelect(_:)), for: .valueChanged)
    datePicker.datePickerMode = .date
    datePicker.preferredDatePickerStyle = .inline
    let toolBar = UIToolbar()
    toolBar.sizeToFit()
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissTextField))
    toolBar.setItems([flexibleSpace, doneButton], animated: false)
    inputAccessoryView = toolBar
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc private func datePickerDidSelect(_ sender: UIDatePicker) {
    date = sender.date
  }
  
  @objc private func dismissTextField() {
    resignFirstResponder()
  }
  
}
