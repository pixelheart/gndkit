//
//  SecureTextField.swift
//  
//
//  Created by Ilham Prabawa on 04/05/23.
//

import SwiftUI

public struct SecureTextField: View {
  
  let title: String
  let placeHolder: String
  
  @State var isEnabled: Bool
  @State var isSecure: Bool = true
  @Binding var value: String
  @Binding var errorMessage: String
  var didChange: (() -> Void)?
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              didChange: (() -> Void)? = nil) {
    
    self.isEnabled = true
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self._errorMessage = .constant("")
    self.didChange = didChange
  }
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              errorMessage: Binding<String>,
              didChange: (() -> Void)? = nil) {
    
    self.isEnabled = true
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self._errorMessage = errorMessage
    self.didChange = didChange
  }
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              errorMessage: Binding<String>,
              isEnabled: Bool,
              didChange: (() -> Void)? = nil) {
    
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self.isEnabled = isEnabled
    self._errorMessage = errorMessage
    self.didChange = didChange
  }
  
  public var body: some View {
    
    ZStack {
      VStack(alignment: .leading) {
        Text(title)
          .font(Font(UIFont.dmSansFont(style: .body(size: 14))))
        
        textField()
          .overlay(
            HStack {
              Spacer()
              Image(isSecure ? "eye_closed" : "eye_opened", bundle: .module)
                .onTapGesture {
                  isSecure = !isSecure
                }
            }.padding(.trailing, 16)
          )
        
        if !errorMessage.isEmpty {
          Text(errorMessage)
            .foregroundColor(.red)
            .captionStyle(size: 12)
        }
      }
      
    }
  }
  
  @ViewBuilder
  func textField() -> some View {
    if isSecure {
      SecureField(placeHolder, text: $value)
        .font(Font(UIFont.dmSansFont(style: .caption(size: 14))))
        .frame(height: 42)
        .padding(.horizontal, 8)
        .background(
          ZStack {
            Color(UIColor.textFieldBackgroundColor)
            if !errorMessage.isEmpty {
              RoundedRectangle(cornerRadius: 8).stroke(Color.red, lineWidth: 2)
            }
          }
        )
        .clipShape(RoundedRectangle(cornerRadius: 8))
        .disabled(!isEnabled)
        .onChange(of: value) { newValue in
          didChange?()
        }
    }else {
      TextField(placeHolder, text: $value)
        .font(Font(UIFont.dmSansFont(style: .caption(size: 14))))
        .frame(height: 42)
        .padding(.horizontal, 8)
        .background(
          ZStack {
            Color(UIColor.textFieldBackgroundColor)
            if !errorMessage.isEmpty {
              RoundedRectangle(cornerRadius: 8).stroke(Color.red, lineWidth: 2)
            }
          }
        )
        .clipShape(RoundedRectangle(cornerRadius: 8))
        .disabled(!isEnabled)
        .onChange(of: value) { newValue in
          errorMessage = ""
          didChange?()
        }
    }
    
  }
  
}

struct SecureTextField_Previews: PreviewProvider {
  static var previews: some View {
    SecureTextField(title: "",
                    placeHolder: "",
                    value: .constant(""))
  }
}
