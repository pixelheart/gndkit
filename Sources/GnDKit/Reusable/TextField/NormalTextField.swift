//
//  NormalTextField.swift
//  
//
//  Created by Ilham Prabawa on 17/04/23.
//

import UIKit
import Combine

public class NormalTextField: FileOwnerNibView {
  
  @IBOutlet weak var textField: UITextField?
  @IBOutlet weak var label: UILabel?
  
  public var obEditing = CurrentValueSubject<Bool, Never>(true)
  
  public var value: String {
    return textField?.text ?? ""
  }
  
  public var placeholder: String? = nil {
    didSet {
      textField?.placeholder = placeholder
    }
  }
  
  public override func setupBundle() {
    bundleOrNil = .module
  }
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    
    textField?.textStyle()
    textField?.delegate = self
    textField?.layer.cornerRadius = 8
    
  }
  
  public func addBorder(with color: UIColor){
    textField?.addBorder(color: color)
  }
  
  public func removeBorder(){
    textField?.removeBorder()
  }
  
  public func setError(with text: String) {
    let attributedText = NSAttributedString(
      string: text,
      attributes: [
        NSAttributedString.Key.font: UIFont.dmSansFont(style: .body(size: 12)),
        NSAttributedString.Key.foregroundColor: UIColor.red
      ]
    )
    label?.attributedText = attributedText
  }
  
}

extension NormalTextField: UITextFieldDelegate {
  
  public func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    obEditing.send(true)
    return true
  }
  
}

