//
//  PasswordTextField.swift
//  
//
//  Created by Ilham Prabawa on 17/04/23.
//

import UIKit
import Combine

public class PasswordTextField: FileOwnerNibView {
  
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var errorLabel: UILabel!
  
  public var obEditing = CurrentValueSubject<Bool, Never>(true)
  public var obSecured = CurrentValueSubject<Bool, Never>(true)
  public var value: String {
    return textField.text ?? ""
  }
  private var isSecured = true
  
  private var subscriptions = Set<AnyCancellable>()
  
  public var placeholder: String? = nil {
    didSet {
      textField?.placeholder = placeholder
    }
  }
  
  public override func setupBundle() {
    bundleOrNil = .module
  }
  
  public override func awakeFromNib() {
    super.awakeFromNib()
    
    textField.passwordStyle()
    textField?.delegate = self
    setAction()
    
    obSecured
      .receive(on: DispatchQueue.main)
      .sink { [weak self] isSecure in
        self?.setSecured(isSecure)
      }.store(in: &subscriptions)
    
  }
  
  public func addBorder(with color: UIColor){
    textField?.addBorder(color: color)
  }
  
  public func removeBorder(){
    textField?.removeBorder()
  }
  
  public func setSecured(_ secured: Bool) {
    textField.isSecureTextEntry = secured
    iconImageView.image = secured
    ? UIImage(named: "eye_closed", in: .module, compatibleWith: .none)
    : UIImage(named: "eye_opened", in: .module, compatibleWith: .none)
  }
  
  public func setError(with text: String) {
    let attributedText = NSAttributedString(
      string: text,
      attributes: [
        NSAttributedString.Key.font: UIFont.dmSansFont(style: .body(size: 12)),
        NSAttributedString.Key.foregroundColor: UIColor.red
      ]
    )
    
    errorLabel?.attributedText = attributedText
  }
  
  private func setAction() {
    iconImageView.isUserInteractionEnabled = true
    iconImageView.addGestureRecognizer(
      UITapGestureRecognizer(
        target: self,
        action: #selector(didTapIconImage)
      )
    )
  }

  @objc
  func didTapIconImage() {
    isSecured = !self.isSecured
    obSecured.send(self.isSecured)
  }

}

extension PasswordTextField: UITextFieldDelegate {
  
  public func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    
    obEditing.send(true)
    
    return true
  }
  
}
