//
//  TextView.swift
//
//
//  Created by Ilham Prabawa on 12/06/23.
//

import SwiftUI

public struct TextView: UIViewRepresentable {
  @Binding var text: String
  let textStyle: UIFont
  let backgroundColor: UIColor
  let textColor: UIColor

  public init(
    text: Binding<String>,
    textStyle: UIFont,
    textColor: UIColor = UIColor.darkText,
    backgroundColor: UIColor = UIColor.systemBackground
  ) {
    self._text = text
    self.textStyle = textStyle
    self.backgroundColor = backgroundColor
    self.textColor = textColor
  }

  public func makeUIView(context: Context) -> UITextView {
    let textView = UITextView()
    textView.font = textStyle
    textView.autocapitalizationType = .sentences
    textView.isSelectable = true
    textView.delegate = context.coordinator
    textView.isEditable = true
    textView.isUserInteractionEnabled = true
    textView.backgroundColor = backgroundColor
    textView.textColor = textColor

    return textView
  }

  public func updateUIView(_ uiView: UITextView, context: Context) {
    if uiView.text != text {
      uiView.text = text
    }
  }

  public func makeCoordinator() -> Coordinator {
    Coordinator(text: $text)
  }

  @MainActor
  public class Coordinator: NSObject, UITextViewDelegate {
    @Binding var text: String

    public init(text: Binding<String>) {
      self._text = text
    }

    public func textViewDidChange(_ textView: UITextView) {
      if Thread.isMainThread {
        self.text = textView.text
      } else {
        DispatchQueue.main.async {
          self.text = textView.text
        }
      }
    }
  }
}
