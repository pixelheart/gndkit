//
//  PTextField.swift
//  
//
//  Created by Ilham Prabawa on 27/04/23.
//

import SwiftUI

public struct PTextField: View {
  
  let title: String
  let placeHolder: String
  var isEnabled: Bool
  @Binding var value: String
  @Binding var errorMessage: String
  var didChange: (() -> Void)?
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              didChange: (() -> Void)? = nil) {
    
    self.isEnabled = true
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self._errorMessage = .constant("")
    self.didChange = didChange
  }
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              errorMessage: Binding<String>,
              didChange: (() -> Void)? = nil) {
    
    self.isEnabled = true
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self._errorMessage = errorMessage
    self.didChange = didChange
  }
  
  public init(title: String,
              placeHolder: String,
              value: Binding<String>,
              errorMessage: Binding<String>,
              isEnabled: Bool,
              didChange: (() -> Void)? = nil) {
    
    self.title = title
    self.placeHolder = placeHolder
    self._value = value
    self.isEnabled = isEnabled
    self._errorMessage = errorMessage
    self.didChange = didChange
  }
  
  public var body: some View {
    
    VStack(alignment: .leading) {
      Text(title)
        .font(Font(UIFont.dmSansFont(style: .body(size: 14))))
      TextField(placeHolder, text: $value)
        .font(Font(UIFont.dmSansFont(style: .caption(size: 14))))
        .frame(height: 42)
        .padding(.horizontal, 8)
        .disabled(!isEnabled)
        .background(
          ZStack {
            Color(UIColor.textFieldBackgroundColor)
            if !errorMessage.isEmpty {
              RoundedRectangle(cornerRadius: 8).stroke(Color.red, lineWidth: 2)
            }
          }
        )
        .clipShape(RoundedRectangle(cornerRadius: 8))
        .onChange(of: value) { newValue in
          didChange?()
        }
      
      if !errorMessage.isEmpty {
        Text(errorMessage)
          .foregroundColor(.red)
          .captionStyle(size: 12)
      }
    }
  }
  
}

struct PTextField_Previews: PreviewProvider {
  static var previews: some View {
    PTextField(title: "Username",
               placeHolder: "Jenengmu",
               value: .constant(""),
               errorMessage: .constant("Error kabeh"))
  }
}
