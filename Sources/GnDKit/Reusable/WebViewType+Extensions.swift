//
//  WebViewType+Extensions.swift
//  AprodhitKit
//
//  Created by Ilham Prabawa on 03/12/24.
//

import Foundation
import Environment

extension WebviewType {
  
  public static func get(type: Self) -> String {
    switch type {
    case .faq:
      return "faq"
    case .privacy:
      return "kebijakan-privasi"
    case .tnc:
      return "syarat-ketentuan"
    }
  }
  
  public static func completeURL(with type: WebviewType) -> String {
    switch Environment.shared.name {
    case .dev:
      return "https://dev.perqara.com/\(WebviewType.get(type: type))?mobile=true"
    case .production:
      return "https://perqara.com/\(WebviewType.get(type: type))?mobile=true"
    case .staging:
      return "https://staging.perqara.com/\(WebviewType.get(type: type))?mobile=true"
    case .local:
      return "https://local.perqara.com/\(WebviewType.get(type: type))?mobile=true"
    case .dev1:
      return "https://dev1.perqara.com/\(WebviewType.get(type: type))?mobile=true"
    }
  }
  
  public static func convert(from txt: String) -> Self {
    switch txt {
    case "tnc":
      return .tnc
    case "privacy":
      return .privacy
    default:
      return .faq
    }
  }
  
}
