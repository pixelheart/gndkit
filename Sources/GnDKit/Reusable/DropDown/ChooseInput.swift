//
//  ChooseInput.swift
//  
//
//  Created by Ilham Prabawa on 13/06/23.
//

import SwiftUI

public struct ChooseInput: View {
  
  let title: String
  @Binding var value: String
  
  public init(title: String, value: Binding<String>) {
    self.title = title
    self._value = value
  }
  
  public var body: some View {
    VStack(alignment: .leading) {
      HStack {
        Text(value.isEmpty ? title : value)
          .captionStyle(size: 14)
        
        Spacer()
        
        Image(systemName: "arrowtriangle.down.fill")
          .resizable()
          .frame(width: 10, height: 10)
          .foregroundColor(Color.lightTextColor)
      }
      .padding(.horizontal, 10)
    }
    .frame(maxWidth: .infinity, minHeight: 42)
    .background(Color.textFieldBackgroundColor)
    .clipShape(RoundedRectangle(cornerRadius: 8))
  }
  
}


struct ChooseInput_Previews: PreviewProvider {
  static var previews: some View {
    ChooseInput(title: "Nama perguruan tinggi", value: .constant(""))
  }
}
