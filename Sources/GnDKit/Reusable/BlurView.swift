//
//  BlurView.swift
//  
//
//  Created by Ilham Prabawa on 27/05/23.
//

import SwiftUI

public struct BlurView: UIViewRepresentable {
  
  var style: UIBlurEffect.Style
  
  public init(style: UIBlurEffect.Style) {
    self.style = style
  }
  
  public func makeUIView(context: Context) -> UIVisualEffectView{
    
    let view = UIVisualEffectView(effect: UIBlurEffect(style: style))
    
    return view
  }
  
  public func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
    
  }
}
