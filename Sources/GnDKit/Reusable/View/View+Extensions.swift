//
//  File.swift
//
//
//  Created by Ilham Prabawa on 12/06/23.
//

import SwiftUI

extension View {

  public func titleStyle(size: CGFloat = 32) -> some View {
    modifier(Title(size: size))
  }

  public func bodyStyle(size: CGFloat = 16) -> some View {
    modifier(Medium(size: size))
  }

  public func captionStyle(size: CGFloat = 14) -> some View {
    modifier(Caption(size: size))
  }

  //  public func adaptsToKeyboard() -> some View {
  //    return modifier(AdaptsToKeyboard())
  //  }

}

extension View {

  public func titleLexend(size: CGFloat = 32) -> some View {
    modifier(SemiBoldLexend(size: size))
  }

  public func bodyLexend(size: CGFloat = 16) -> some View {
    modifier(RegularLexend(size: size))
  }

  public func captionLexend(size: CGFloat = 14) -> some View {
    modifier(LightLexend(size: size))
  }

}
