//
//  Frame.swift
//
//
//  Created by Ilham Prabawa on 26/09/24.
//

import Foundation

public struct Frame {

  public let width: CGFloat
  public let height: CGFloat

  public init(
    width: CGFloat,
    height: CGFloat
  ) {
    self.width = width
    self.height = height
  }

}
