//  Created by Maksim Kalik on 26/05/2022.

import UIKit
import SwiftUI

public struct UniversalLabelView: UIViewRepresentable {

  private(set) var html: String
  @Binding var dynamicHeight: CGFloat
  public var action: (URL) -> Void
  public var mutatingWrapper = MutatingWrapper()

  public init(
    html: String,
    dynamicHeight: Binding<CGFloat>,
    action: @escaping (URL) -> Void
  ) {
    self.html = html
    self._dynamicHeight = dynamicHeight
    self.action = action
  }
  
  public class MutatingWrapper {
    public var font: UIFont
    public var fontSize: CGFloat
    public var textAlignment: NSTextAlignment
    public var numberOfLines: Int
    public var textColor: UIColor
    public var linkColor: UIColor

    public init() {
      self.font = .systemFont(ofSize: 11)
      self.fontSize = 0
      self.textAlignment = .center
      self.numberOfLines = 0
      self.textColor = .black
      self.linkColor = .blue
    }

    public init(
      font: UIFont,
      fontSize: CGFloat,
      textAlignment: NSTextAlignment,
      numberOfLines: Int,
      textColor: UIColor,
      linkColor: UIColor
    ) {
      self.font = font
      self.fontSize = fontSize
      self.textAlignment = textAlignment
      self.numberOfLines = numberOfLines
      self.textColor = textColor
      self.linkColor = linkColor
    }
  }

  public func makeUIView(context: UIViewRepresentableContext<Self>) -> UniversalLabel {
    let label = UniversalLabel()
    label.isUserInteractionEnabled = true

    label.textFont = mutatingWrapper.font
    label.numberOfLines = mutatingWrapper.numberOfLines
    label.textFontSize = mutatingWrapper.fontSize
    label.textAlignment = mutatingWrapper.textAlignment
    label.textColor = mutatingWrapper.textColor
    label.linkColor = mutatingWrapper.linkColor
    label.lineBreakMode = .byWordWrapping

    label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
    label.setContentCompressionResistancePriority(.defaultLow, for: .vertical)

    label.onPress { url in
      guard let url = url else { return }
      action(url)
    }

    return label
  }

  public func updateUIView(_ uiView: UniversalLabel, context: UIViewRepresentableContext<Self>) {
    uiView.html = html
    uiView.numberOfLines = mutatingWrapper.numberOfLines

    DispatchQueue.main.async {
      dynamicHeight = uiView.sizeThatFits(
        CGSize(
          width: uiView.bounds.width,
          height: CGFloat.greatestFiniteMagnitude
        )
      ).height
    }
  }

  public func fontStyle(_ font: UIFont) -> Self {
    mutatingWrapper.font = font
    return self
  }

  public func textFontSize(_ fontSize: CGFloat) -> Self {
    mutatingWrapper.fontSize = fontSize
    return self
  }

  public func textAlignment(_ textAlignment: NSTextAlignment) -> Self {
    mutatingWrapper.textAlignment = textAlignment
    return self
  }

  public func textColor(_ textColor: UIColor) -> Self {
    mutatingWrapper.textColor = textColor
    return self
  }

  public func linkColor(_ linkColor: UIColor) -> Self {
    mutatingWrapper.linkColor = linkColor
    return self
  }

  public func lineLimit(_ number: Int) -> Self {
    mutatingWrapper.numberOfLines = number
    return self
  }
}

