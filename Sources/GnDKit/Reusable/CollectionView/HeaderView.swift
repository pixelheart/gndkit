//
//  HeaderView.swift
//  Testing
//
//  Created by Ilham Prabawa on 05/06/23.
//

import UIKit

public class HeaderModel {
  let id: Int
  let label: String
  var selected: Bool = false
  
  public init(id: Int, label: String, selected: Bool = false) {
    self.id = id
    self.label = label
    self.selected = selected
  }
}

class HeaderView: UICollectionReusableView {
  
  public var model: HeaderModel?{
    didSet{
      guard let m = model else { return }
      label.tag = m.id
      label.text = m.label
    }
  }
  
  public let label: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    return label
  }()
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    backgroundColor = UIColor.red
    addSubview(label)
    label.frame = bounds
    
  }
  
}
