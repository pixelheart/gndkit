//
//  ChildViewCell.swift
//  Testing
//
//  Created by Ilham Prabawa on 05/06/23.
//

import UIKit

class ChildViewCell: UICollectionViewCell {
  
  private let label: UILabel = {
    let label = UILabel()
    label.text = "Mantab"
    label.textAlignment = .center
    return label
  }()
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    contentView.addSubview(label)
    label.frame = contentView.bounds
    
  }
    
}
