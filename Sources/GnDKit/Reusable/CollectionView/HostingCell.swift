//
//  HostingCell.swift
//  theathletic-ios
//
//  Created by .
//  Copyright © 2021 The Athletic. All rights reserved.
//

import SwiftUI
import UIKit

open class HostingCell<Content: View>: UICollectionViewCell {

  private let hostingController = UIHostingController<Content?>(rootView: nil)

  deinit {
    removeHostingControllerFromParent()
  }

  private func removeHostingControllerFromParent() {
    hostingController.willMove(toParent: nil)
    hostingController.view.removeFromSuperview()
    hostingController.removeFromParent()
  }

  open func configure(with view: Content, in parent: UIViewController?) {
    guard let parent = parent else {
      let message = "parent viewController is required"
      assertionFailure(message)
      return
    }

    hostingController.rootView = view
    hostingController.view.invalidateIntrinsicContentSize()

    let requiresControllerMove = hostingController.parent != parent
    if requiresControllerMove {
      removeHostingControllerFromParent()
      parent.addChild(hostingController)
    }

    if !contentView.subviews.contains(hostingController.view) {
      setUpUI()
    }

    if requiresControllerMove {
      hostingController.didMove(toParent: parent)
    }
  }

  private func setUpUI() {
    hostingController.view.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(hostingController.view)
    hostingController.view.backgroundColor = .clear

    NSLayoutConstraint.activate([
      hostingController.view.topAnchor.constraint(
        equalTo: contentView.topAnchor
      ),
      hostingController.view.bottomAnchor.constraint(
        equalTo: contentView.bottomAnchor
      ),
      hostingController.view.leadingAnchor.constraint(
        equalTo: contentView.leadingAnchor
      ),
      hostingController.view.trailingAnchor.constraint(
        equalTo: contentView.trailingAnchor
      ),
    ])
  }
}
