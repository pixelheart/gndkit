//
//  SlidingTabController.swift
//  Osvas-iOS
//
//  Created by Ilham Hadi Prabawa on 9/30/20.
//  Copyright © 2020 Osvas. All rights reserved.
//

import Foundation
import UIKit

public struct TabModel {
  let title: String
  let page: UIViewController
  
  public init(title: String,
              page: UIViewController) {
    
    self.title = title
    self.page = page
  }
}

open class SlidingTabController: NiblessViewController {
  
  private let menuId = "menu_cell"
  private let pageId = "page_cell"
  
  public var tabs = [TabModel]()
  public var horizontalBarBackgroundColor: UIColor = UIColor.clear
  public var menuBarBackground: UIColor = .clear
  
  public var tintColor: UIColor = .clear
  
  var topOffset: CGFloat = 50
  
  private var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
  private let horizontalBarView = UIView()
  
  public lazy var navigationBar: UIView = {
    let navBar = UIView()
    navBar.backgroundColor = .white
    return navBar
  }()
  
  lazy var collectionMenuBar: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.backgroundColor = .white
    cv.dataSource = self
    cv.delegate = self
    cv.showsHorizontalScrollIndicator = false
    cv.register(MenubarCell.self, forCellWithReuseIdentifier: menuId)
    return cv
  }()
  
  lazy var collectionPage: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.backgroundColor = .white
    cv.dataSource = self
    cv.delegate = self
    cv.isPagingEnabled = true
    cv.showsHorizontalScrollIndicator = false
    cv.register(PageContainerCell.self, forCellWithReuseIdentifier: pageId)
    return cv
  }()
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    
    setupNavBar()
    
  }
  
  open func setupNavBar() {
    view.addSubview(navigationBar)
    navigationBar.anchor(
      top: view.topAnchor,
      leading: view.leadingAnchor,
      bottom: nil,
      trailing: view.trailingAnchor
    )
  }
  
  public func build(){
    
    collectionMenuBar.backgroundColor = menuBarBackground
    
    view.addSubview(collectionPage)
    view.addSubview(collectionMenuBar)
    
    collectionMenuBar.anchor(top: navigationBar.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: CGSize(width: view.frame.width, height: 50))
    
    collectionPage.anchor(top: collectionMenuBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
    
    horizontalBarView.backgroundColor = horizontalBarBackgroundColor
    horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(horizontalBarView)
    
    horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: view.leftAnchor)
    horizontalBarLeftAnchorConstraint?.isActive = true
    
    horizontalBarView.anchor(top: nil, leading: nil, bottom: collectionMenuBar.bottomAnchor, trailing: nil, size: CGSize(width: view.frame.width / CGFloat(tabs.count), height: 3))
    
  }
  
  open func scrollPage(to index: Int) {
    let rect = self.collectionPage.layoutAttributesForItem(at: IndexPath(row: index, section: 0))?.frame
    self.collectionPage.scrollRectToVisible(rect!, animated: true)
    self.collectionPage.collectionViewLayout.invalidateLayout()
  }

}

extension SlidingTabController: UICollectionViewDataSource {
  
  public func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    if collectionView == collectionPage {
      return tabs.count
    }
    
    return tabs.count
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    
    if collectionView == collectionPage {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pageId, for: indexPath) as! PageContainerCell
      cell.vc = tabs[indexPath.row].page
      return cell
    }
    
    let barCell = collectionView.dequeueReusableCell(withReuseIdentifier: menuId, for: indexPath) as! MenubarCell
    barCell.item = tabs[indexPath.row].title
    barCell.label.textColor = tintColor
    return barCell
  }
  
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(tabs.count)
  }
}

extension SlidingTabController: UICollectionViewDelegate {
  
  public func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    if collectionView == collectionMenuBar {
      collectionPage.setContentOffset(CGPoint(x: self.view.frame.width * CGFloat(indexPath.item), y: 0), animated: true)
    }
  }
}

extension SlidingTabController: UICollectionViewDelegateFlowLayout {
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    
    if collectionView == collectionPage {
      return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    return CGSize(width: view.frame.width / CGFloat(tabs.count), height: 50)
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return 0
  }
  
  public func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt section: Int
  ) -> CGFloat {
    return 0
  }
  
}

class MenubarCell: UICollectionViewCell {
  
  let label = UILabel(text: "Test",
                      font: .systemFont(ofSize: 14, weight: .bold),
                      textColor: .black,
                      textAlignment: .center,
                      numberOfLines: 2)
  
  var item: String? {
    didSet {
      label.text = item
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    contentView.stack(label).centerInSuperview()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}


class PageContainerCell: UICollectionViewCell {
  
  var vc: UIViewController? {
    didSet{
      contentView.stack(vc?.view ?? UIView())
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = .white
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
