//
//  UIFont+CustomFont.swift
//  Ilham Hadi Prabawa
//
//  Created by Ilham Hadi Prabawa on 14/02/22.
//  Copyright © 2022 Xamben. All rights reserved.
//

import Foundation
import UIKit

public enum FontStyle {
  case title(size: CGFloat)
  case body(size: CGFloat)
  case caption(size: CGFloat)
}

public extension UIFont {
  
  static func dmSansFont(style: FontStyle) -> UIFont {
    switch style {
    case .title(let size):
      guard let customFont = UIFont(name: "DMSans-Bold", size: size)
      else {
        return .systemFont(ofSize: size, weight: .bold)
      }
      return customFont
    case .body(let size):
      guard let customFont = UIFont(name: "DMSans-Medium", size: size)
      else {
        return .systemFont(ofSize: size, weight: .medium)
      }
      return customFont
    case .caption(let size):
      guard let customFont = UIFont(name: "DMSans-Regular", size: size)
      else {
        return .systemFont(ofSize: size, weight: .regular)
      }
      return customFont
    }
  }
  
}

public extension UIFont {

  static func lexendFont(style: FontStyle) -> UIFont {
    switch style {
    case .title(let size):
      guard let customFont = UIFont(name: "Lexend-SemiBold", size: size)
      else {
        return .systemFont(ofSize: size, weight: .bold)
      }
      return customFont
    case .body(let size):
      guard let customFont = UIFont(name: "Lexend-Regular", size: size)
      else {
        return .systemFont(ofSize: size, weight: .medium)
      }
      return customFont
    case .caption(let size):
      guard let customFont = UIFont(name: "Lexend-Light", size: size)
      else {
        return .systemFont(ofSize: size, weight: .regular)
      }
      return customFont
    }
  }

}
