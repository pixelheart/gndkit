//
//  Color.swift
// 
//
//  Created by Ilham Prabawa on 30/03/23.
//
import UIKit
import SwiftUI

extension UIColor {
  public static let primary50 = UIColor(0xEDF2F7)
  public static let primary700 = UIColor(0x0A50A3)
  public static let secondary = UIColor(0xEDF2F7)
  public static let background = UIColor(0xFFFFFF)
  public static let lightTrim = UIColor(0xFFFFFF)
  public static let darkTextColor = UIColor(0x333333)
  public static let lightTextColor = UIColor(0x676767)
  public static let indicatorColor = UIColor(0x0066DF)
  public static let buttonActiveColor = UIColor(0x0A50A3)
  public static let textFieldBackgroundColor = UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)
  public static let errorColor = UIColor(0xFDEEEC)
  public static let errorSnackbarTextColor = UIColor(0x752C21)
  public static let OTPColor = UIColor(0xFCFCFC)
  public static let OTPBorderColor = UIColor(0xEFEFEF)
  public static let meMessageColor = UIColor(0xDDF2FA)
  public static let chatBackground = UIColor(0xF5F5F5)
  public static let greenSnackbarColor = UIColor(0xEAFAF0)
  public static let greenSnackbarTextColor = UIColor(0x156633)
  public static let greenCallImageBorder = UIColor(0x00E092)
  public static let redCallButtonReject = UIColor(0xFF0000)
  public static let gray050 = UIColor(0xF6F9FA)
  public static let gray200 = UIColor(0xC6CEDC)
  public static let gray600 = UIColor(red: 0.33, green: 0.44, blue: 0.48, alpha: 1)
  public static let gray500 = UIColor(0x647387)
  public static let gray700 = UIColor(0x39424D)
  public static let gray300 = UIColor(0xFF0000)
  public static let gray100 = UIColor(0xE7ECF0)
  public static let primary050 = UIColor(0xF5FBFD)
  public static let primary500 = UIColor(0x2072B8)
  public static let primary200 = UIColor(0xB1E0F1)
  public static let green80 = UIColor(0x4CAF50)
  public static let green40 = UIColor(0xD3F1D4)
  public static let successColor = UIColor(0x209D4E)
  public static let success200 = UIColor(0xCAF4D9)
  public static let success500 = UIColor(0x2AD168)
  public static let success050 = UIColor(0xF4FDF7)
  public static let titleColor = UIColor(red: 38/255, green: 38/255, blue: 1/255)
}

extension Color {
  public static let primary50 = Color(hex: 0xEDF2F7)
  public static let primary700 = Color(hex: 0x0A50A3)
  public static let secondary = Color(hex: 0xEDF2F7)
  public static let lightGrayBg = Color(hex: 0xDFDFDF)
  public static let skillText = Color(hex: 0xB1E0F1)
  public static let background = Color(hex: 0xFFFFFF)
  public static let lightTrim = Color(hex: 0xFFFFFF)
  public static let darkTextColor = Color(hex: 0x333333)
  public static let lightTextColor = Color(hex: 0x676767)
  public static let buttonActiveColor = Color(hex: 0x0A50A3)
  public static let textFieldBackgroundColor = Color(red: 0.961, green: 0.961, blue: 0.961)
  public static let errorColor = Color(hex: 0xFDEEEC)
  public static let OTPColor = Color(hex: 0xFCFCFC)
  public static let OTPBorderColor = Color(hex: 0xEFEFEF)
  public static let toggleActive = Color(hex: 0x4CAF50)
  public static let toggleInnactive = Color(hex: 0x4CAF50)
  public static let searchColor = Color(hex: 0xF5F5F5)
  public static let tagTextColor = Color(hex: 0x6A3700)
  public static let tagColor = Color(hex: 0xFEF9D9)
  public static let incomingColor = Color(hex: 0xEAFAF0)
  public static let textSendWarning = Color(hex: 0x784D05)
  public static let bgSendWarning = Color(hex: 0xFFF8D4)
  public static let redBackground = Color(hex: 0xD7503D)
  public static let btnBackground = Color(hex: 0xCBCBCB)
  public static let btnRed = Color(hex: 0xEF5944)
  public static let bannerColor = Color(hex: 0xFFF8D4)
  public static let meMessageColor = Color(hex: 0xDDF2FA)
  public static let chatBackground = Color(hex: 0xF5F5F5)
  public static let primary050 = Color(hex: 0xF5FBFD)
  public static let primary500 = Color(hex: 0x2072B8)
  public static let primary200 = Color(hex: 0xB1E0F1)
  public static let greenCallBorder = Color(hex: 0x00E092)
  public static let redCallButtonReject = Color(hex: 0xFF0000)
  public static let darkGray400 = Color(hex: 0x757E8C)
  public static let darkGray300 = Color(hex: 0xB0B5BD)
  public static let gray100 = Color(hex: 0xEBEDEF)
  public static let offlineStateBorderColor = Color(hex: 0xD6D7D7)
  public static let onlineStateColor = Color(hex: 0x757E8C)
  public static let green80 = Color(hex: 0x4CAF50)
  public static let green40 = Color(hex: 0xD3F1D4)
  public static let successColor = Color(hex: 0x209D4E)
  public static let success200 = Color(hex: 0xCAF4D9)
  public static let success500 = Color(hex: 0x2AD168)
  public static let success050 = Color(hex: 0xF4FDF7)
  public static let titleColor = Color(red: 38/255, green: 38/255, blue: 1/255)
}

extension Color {
  public init(hex: UInt, alpha: Double = 1) {
    self.init(
      .sRGB,
      red: Double((hex >> 16) & 0xff) / 255,
      green: Double((hex >> 08) & 0xff) / 255,
      blue: Double((hex >> 00) & 0xff) / 255,
      opacity: alpha
    )
  }
}
