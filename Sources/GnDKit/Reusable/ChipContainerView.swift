//
//  ChipContainerView.swift
//  
//
//  Created by Ilham Prabawa on 04/06/23.
//

import SwiftUI

public struct ChipData: Identifiable, Hashable {
  public var id = UUID().uuidString
  public var text: String
  let color: Color
  var isExceeded: Bool = false
  
  public init(text: String) {
    self.text = text
    self.color = .white
  }

  public init(text: String,
              color: Color = .white) {
    self.text = text
    self.color = color
  }
}

public typealias OnChangeHeight = ((CGFloat) -> Void)?

public struct ChipContainerView: View {
  @Binding var chips: [ChipData]
  public var action: OnChangeHeight?
  
  public init(chips: Binding<[ChipData]>) {
    self._chips = chips
  }
  
  public func onChangeHeight(perform action: OnChangeHeight) -> Self {
    var copy = self
    copy.action = action
    return copy
  }
  
  public var body: some View {
    var width = CGFloat.zero
    var height = CGFloat.zero
    
    return GeometryReader { geo in
      ZStack(alignment: .topLeading) {
        ForEach(chips) { data in
          ChipView(title: data.text, color: data.color)
            .padding(.all, 5)
            .alignmentGuide(.leading) { dimension in
              if(abs(width - dimension.width) > geo.size.width) {
                width = 0
                height -= dimension.height
              }

              let result = width
              if data.id == chips.last!.id {
                width = 0
              }else {
                width -= dimension.width
              }

              return result
            }
            .alignmentGuide(.top) { dimension in

              if data.id == chips.last!.id {
                height = 0
              }

              let result = height
              return result
            }
        }
      }
      .background(GeometryReader { gp -> Color in
        DispatchQueue.main.async {
          if let action = self.action {
            action?(gp.size.height)
          }
        }
        return Color.clear
      })
      
    }
  }
}

struct ChipContainerView_Previews: PreviewProvider {
  static var previews: some View {
    ChipContainerView(chips: .constant([]))
  }
}

struct ChipView: View {
  let title: String
  let color: Color

  var body: some View {
    HStack(spacing: 4) {
      Text(title)
        .font(Font(UIFont.dmSansFont(style: .body(size: 12))))
        .foregroundColor(Color.buttonActiveColor)
        .lineLimit(1)
    }
    .padding(.vertical, 6)
    .padding(.leading, 6)
    .padding(.trailing, 6)
    .background(color)
    .cornerRadius(8)

  }
}
