//
//  Snackbar.swift
//
//  Created by Ilham Hadi Prabawa on 10/20/21.
//

import Foundation
import UIKit
import Combine

public class SnackbarManager{
  
  private weak var topConstraint: NSLayoutConstraint!
  private var parent: UIViewController?
  private var snackbar: Snackbar?
  private var isShowing: Bool = false
  private var shouldShow = PassthroughSubject<Bool, Never>()
  
  //MARK: - view properties
  private var message: String = ""
  private var color: UIColor = UIColor.darkTextColor
  private var textColor: UIColor = .white
  
  public init() {
    snackbar = Snackbar()
  }
  
  func setupConstraint() {
    
    guard let parent = parent,
          let snackbar = snackbar else {
      return
    }
    
    topConstraint = snackbar.topAnchor.constraint(equalTo: parent.view.topAnchor, constant: -50)
    NSLayoutConstraint.activate([
      snackbar.leadingAnchor.constraint(equalTo: parent.view.leadingAnchor, constant: 20),
      snackbar.trailingAnchor.constraint(equalTo: parent.view.trailingAnchor, constant: -20),
      snackbar.heightAnchor.constraint(equalToConstant: 40),
      topConstraint
    ])
  }
  
  public func show(){
    
    guard let parent = parent,
          let snackbar = snackbar else {
      return
    }
    
    if parent.view.subviews.contains(snackbar) {
      snackbar.removeFromSuperview()
    }
    
    //Add to view
    parent.view.addSubview(snackbar)
    snackbarConfiguration()
    setupConstraint()
    parent.view.layoutIfNeeded()
    
    if !isShowing {
      animateShowing()
    }
    
  }

  //MARK: - Configuration
  public func setParent(_ vc: UIViewController) -> SnackbarManager{
    self.parent = vc
    return self
  }
  
  public func setMessage(_ message: String) -> SnackbarManager{
    self.message = message
    return self
  }
  
  public func setColor(_ color: UIColor) -> SnackbarManager {
    self.color = color
    return self
  }
  
  public func setTextColor(_ color: UIColor) -> SnackbarManager {
    self.textColor = color
    return self
  }
  
  public func setShow(_ state: PassthroughSubject<Bool, Never>) -> SnackbarManager{
    self.shouldShow = state
    return self
  }
  
  //MARK: - Private method
  private func animateShowing() {
    guard let parent = parent else {
      return
    }
    
    indicateShow()
    UIView.animate(withDuration: 0.8,
                   delay: 0.2,
                   usingSpringWithDamping: 0.4,
                   initialSpringVelocity: 1) { [weak self] in
      
      self?.topConstraint.constant = 100
      parent.view.layoutIfNeeded()
      
    } completion: { [weak self] _ in
      
      UIView.animate(withDuration: 0.3, delay: 0.5) {
        
        self?.topConstraint.constant = -50
        parent.view.layoutIfNeeded()
        
      } completion: { [weak self] _ in
        guard let self = self else { return }
        self.shouldShow.send(false)
        self.indicateHide()
      }
      
    }
  }
  
  private func snackbarConfiguration() {
    snackbar?.setMessage(message)
    snackbar?.setTextColor(textColor)
    snackbar?.setColor(color)
  }
  
  private func removeSnackbar() {
    isShowing = false
    parent?.view.subviews
      .filter{ $0 == snackbar }
      .forEach{ v in
        v.removeFromSuperview()
      }

    snackbar = nil
    parent = nil
  }
  
  private func indicateShow() {
    isShowing = true
  }
  
  private func indicateHide() {
    isShowing = false
    removeSnackbar()
  }

  deinit {
    print("deinit Snackbar Manager")
  }
  
}
