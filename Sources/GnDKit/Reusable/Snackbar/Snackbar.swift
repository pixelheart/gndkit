//
//  Snackbar.swift
//  
//
//  Created by Ilham Prabawa on 06/04/23.
//

import UIKit

class Snackbar: NiblessView {
  
  private let label: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  init() {
    super.init(frame: .zero)
    backgroundColor = UIColor.darkTextColor
    translatesAutoresizingMaskIntoConstraints = false
    layer.cornerRadius = 8
    setupConstraint()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    addShadow(cornerRadius: 8,
              shadowRadius: 10,
              shadowOffset: .init(width: 5, height: 5),
              color: backgroundColor!.withAlphaComponent(0.5))
  }
  
  private func setupConstraint() {
    addSubview(label)
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: centerXAnchor),
      label.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])
  }
  
  public func setMessage(_ message: String) {
    label.text = message
  }
  
  public func setTextColor(_ color: UIColor) {
    label.textColor = color
  }
  
  public func setColor(_ color: UIColor) {
    backgroundColor = color
  }
  
  deinit {
    print("deinit Snackbar")
  }
  
}
