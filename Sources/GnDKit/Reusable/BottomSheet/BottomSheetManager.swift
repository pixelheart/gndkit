//
//  BottomSheetManager.swift
//  
//
//  Created by Ilham Prabawa on 02/05/23.
//

import UIKit
import Combine

public protocol BottomSheetManagerDelegate: AnyObject {
  func presentSheet()
  func dismissSheet()
}

open class BottomSheetManager: ContentViewDelegate {

  public var navigationController: UINavigationController!
  public var parentController: UIViewController!
  private var contentController: BottomSheetContentController!
  
  public var shouldShow = PassthroughSubject<Bool, Never>()
  public var subscriptions = Set<AnyCancellable>()
  
  //delegate
  public weak var delegate: BottomSheetManagerDelegate?
  
  public init(navigationController: UINavigationController!,
              parentController: UIViewController!) {
    
    self.navigationController = navigationController
    self.parentController = parentController
    
  }
  
  public func setController(controller: BottomSheetContentController) -> BottomSheetManager{
    self.contentController = controller
    return self
  }
  
  public func navigationShow() {
    contentController.contentView.handleArea.isUserInteractionEnabled = false
    contentController.modalPresentationStyle = .overFullScreen
    navigationController.present(self.contentController, animated: true)
  }
  
  public func show() {
    guard let _ = contentController else { return }
    contentController.setupBottomSheet(toParent: navigationController)
    contentController.delegate = self
    contentController.contentView.handleArea.isUserInteractionEnabled = true
    UIView.animate(
      withDuration: 0.2,
      delay: 0,
      options: .curveEaseIn,
      animations: {
        self.parentController.view.layoutIfNeeded()
      }
    )
  }
  
  public func hide() {
    guard let controller = contentController else { return }
    controller.animateCardDismiss()
  }
  
  public func releaseBottomSheet() {
    guard let _ = contentController else { return }
    self.contentController.willMove(toParent: nil)
    self.contentController.view.removeFromSuperview()
    self.contentController.removeFromParent()
    self.contentController = nil
  }

  open func dismissed() {
    releaseBottomSheet()
    delegate?.dismissSheet()
  }

  deinit {
    print("deinit", String(describing: BottomSheetManager.self))
  }
  
}
