//
//  FixedHeightBottomSheet.swift
//  
//
//  Created by Ilham Prabawa on 20/06/23.
//

import SwiftUI

public struct FixedHeightBottomSheet<Content>: View where Content: View{
  
  @Binding var presented: Bool
  var content: () -> Content
  var showState: (Bool) -> Void
  var isFixedHeight: Bool = false
  var offsetY: CGFloat = 0
  //Gesture Offset
  @State var offset: CGFloat = 0
  
  
  public init(presented: Binding<Bool>,
              content: @escaping () -> Content,
              showState: @escaping (Bool) -> Void,
              isFixedHeight: Bool = false,
              offsetY: CGFloat = 0) {
    
    self._presented = presented
    self.content = content
    self.showState = showState
    self.isFixedHeight = isFixedHeight
    self.offsetY = offsetY
  }
  
  public var body: some View {
    ZStack {
      Color.black.opacity(presented ? 0.4 : 0)
        .animation(.default, value: presented)
      
      VStack {
        VStack {
          Capsule()
            .frame(width: 50, height: 5)
            .foregroundColor(Color.darkTextColor)
            .position(x: screen.width/2, y: 20)
        }.frame(height: 40)
        
        content()
      }
      .frame(maxHeight: .infinity, alignment: .top)
      .background(Color.white)
      .clipShape(CustomCorner(corners: [.topLeft, .topRight], radius: 16))
      .offset(y: offset)
      .gesture(DragGesture().onChanged(onChange(value:)).onEnded(onEnded(value:)))
      .offset(y: presented ? setupOffsetY() : screen.height)
      .animation(
        Animation.spring(response: 0.5,
                         dampingFraction: 0.6,
                         blendDuration: 1),
        value: presented
      )
    }.edgesIgnoringSafeArea(.all)
  }
    
  func setupOffsetY() -> CGFloat {
    return isFixedHeight ? offsetY : screen.height/3
  }
  
  func onChange(value: DragGesture.Value) {
    if value.translation.height > 0 {
      offset = value.translation.height
    }
  }
  
  func onEnded(value: DragGesture.Value) {
    if value.translation.height > 0 {
      withAnimation(Animation.easeIn(duration: 0.2)) {
        let height = screen.height/3
        if value.translation.height > height / 1.5 {
          presented.toggle()
          showState(presented)
        }
        
        offset = 0
      }
    }
  }
  
}


struct FixedHeightBottomSheet_Previews: PreviewProvider {
  static var previews: some View {
    FixedHeightBottomSheet(presented: .constant(false)) {
      Text("")
    } showState: { _ in }
  }
}
