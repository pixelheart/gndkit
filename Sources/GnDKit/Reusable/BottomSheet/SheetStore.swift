//
//  SheetStore.swift
//  
//
//  Created by Ilham Prabawa on 29/06/23.
//

import Foundation
import Combine

open class SheetStore {
  
  public var obsText = PassthroughSubject<String, Never>()
  public var isLoading = PassthroughSubject<Bool, Never>()
  public var subscriptions = Set<AnyCancellable>()
  
  public required init() {}
  
  open func request() {}
  
  open func indicateLoading() {
    isLoading.send(true)
  }
  
  open func indicateError() {
    isLoading.send(false)
  }
  
  open func indicateSuccess() {
    isLoading.send(false)
  }

  deinit {
    print("deinit", String(describing: SheetStore.self))
  }

}
