//
//  BottomSheet.swift
//
//
//  Created by Ilham Prabawa on 07/11/20.
//

import SwiftUI

public struct BottomSheet<Content>: View where Content: View{
  
  @Binding var presented: Bool
  var content: () -> Content
  var showState: (Bool) -> Void
  var edges = UIApplication.shared.windows.first?.safeAreaInsets
  
  @State var offset : CGFloat = 0
  
  public init(presented: Binding<Bool>,
              content: @escaping () -> Content,
              showState: @escaping (Bool) -> Void) {
    
    self._presented = presented
    self.content = content
    self.showState = showState
  }
  
  public var body: some View {
    
    VStack{
      
      Spacer()
      
      VStack(spacing: 12){
        
        Capsule()
          .fill(Color.gray)
          .frame(width: 60, height: 4)
        
        content()
          .frame(height: screen.height - 300)
          .contentShape(Rectangle())
          .padding(.bottom, 50)
      }
      .padding(.top)
      .background(
        Color.white
          .clipShape(CustomCorner(corners: [.topLeft, .topRight], radius: 16))
      )
      .offset(y: offset)
      // bottom sheet remove swipe gesture....
      .gesture(DragGesture().onChanged(onChanged(value:)).onEnded(onEnded(value:)))
      .offset(y: presented ? 0 : screen.height)
      .animation(.interactiveSpring(response: 0.5), value: presented)
    }
    .ignoresSafeArea()
//    .background(
//      Color.black.opacity(presented ? 0.3 : 0).ignoresSafeArea()
//        .onTapGesture{
//          withAnimation{ presented.toggle() }
//        }
//    )
  }
  
  func onChanged(value: DragGesture.Value){
    
    if value.translation.height > 0{
      
      offset = value.translation.height
    }
  }
  
  func onEnded(value: DragGesture.Value){
    
    if value.translation.height > 0{
      
      withAnimation(Animation.easeIn(duration: 0.2)){
        
        // checking.....
        
        let height = UIScreen.main.bounds.height / 3
        
        if value.translation.height > height / 1.5{
          
          presented.toggle()
        }
        offset = 0
      }
    }
  }
}

