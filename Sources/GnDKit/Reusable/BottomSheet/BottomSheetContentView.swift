//
//  ContentView.swift
//
//
//  Created by Ilham Prabawa on 28/06/23.
//

import Foundation
import UIKit
import Combine
import JGProgressHUD

open class BottomSheetContentView: NiblessView {

  public let store: SheetStore

  public let capsuleView: UIView = {
    let area = UIView()
    area.translatesAutoresizingMaskIntoConstraints = false
    area.backgroundColor = .darkGray
    area.layer.cornerRadius = 5
    return area
  }()

  public let handleArea: UIView = {
    let area = UIView()
    area.translatesAutoresizingMaskIntoConstraints = false
    area.backgroundColor = .clear
    return area
  }()

  public let contentView : UIScrollView = {
    let sv = UIScrollView()
    sv.backgroundColor = .clear
    sv.showsVerticalScrollIndicator = true
    sv.translatesAutoresizingMaskIntoConstraints = false
    return sv
  }()

  var progressHUD: JGProgressHUD?

  public var subscriptions = Set<AnyCancellable>()

  required public init(store: SheetStore) {
    self.store = store
    super.init(frame: .zero)

    backgroundColor = .white
    setup()
    setupView()
  }

  open override func didMoveToWindow() {
    super.didMoveToWindow()
  }

  open func setup() {}

  open func setupView() {
    self.translatesAutoresizingMaskIntoConstraints = false

    addSubview(handleArea)
    NSLayoutConstraint.activate([
      handleArea.topAnchor.constraint(equalTo: topAnchor),
      handleArea.leadingAnchor.constraint(equalTo: leadingAnchor),
      handleArea.trailingAnchor.constraint(equalTo: trailingAnchor),
      handleArea.heightAnchor.constraint(equalToConstant: 80)
    ])

    handleArea.addSubview(capsuleView)
    NSLayoutConstraint.activate([
      capsuleView.centerXAnchor.constraint(equalTo: handleArea.centerXAnchor),
      capsuleView.topAnchor.constraint(equalTo: handleArea.topAnchor, constant: 15),
      capsuleView.widthAnchor.constraint(equalToConstant: 80),
      capsuleView.heightAnchor.constraint(equalToConstant: 10)
    ])

    addSubview(contentView)
    NSLayoutConstraint.activate([
      contentView.topAnchor.constraint(equalTo: capsuleView.safeAreaLayoutGuide.bottomAnchor, constant: 20),
      contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
      contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
      contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }

  open func showLoading() {
    progressHUD = JGProgressHUD(style: .dark)
    progressHUD?.show(in: self, animated: true, afterDelay: 0.3)
  }

  open func hideLoading() {
    progressHUD?.dismiss(animated: true)
    progressHUD?.removeFromSuperview()
  }

  deinit {
    print("deinit", String(describing: BottomSheetContentView.self))
  }

}
