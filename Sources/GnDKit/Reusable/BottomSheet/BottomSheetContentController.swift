//
//  ContentController.swift
//  
//
//  Created by Ilham Prabawa on 28/06/23.
//

import Foundation
import UIKit
import Combine

public protocol ContentViewDelegate: AnyObject {
  func dismissed()
}

open class BottomSheetContentController: NiblessViewController {
  
  private var store: SheetStore!
  private let cardHandleAreaHeight: CGFloat = 65
  
  public var visualEffectView: UIVisualEffectView!
  public var contentView: BottomSheetContentView!
  public var actualHeight = CurrentValueSubject<CGFloat, Never>(200)
  public var fixedHeight: CGFloat = 0
  public var useFixedHeight: Bool = false
  private var heightConstraint: NSLayoutConstraint!
  private var bottomConstraint: NSLayoutConstraint!
  private var defaultCardHeight: CGFloat = 0
  private var minimumCardHeight: CGFloat = 200
  private var newHeight: CGFloat = 0
  private var currentContainerHeight: CGFloat = 0
  private var isDismissable: Bool = true

  lazy var dimmedView: UIView = {
    let v = UIView()
    v.backgroundColor = .black
    v.alpha = 0
    v.isUserInteractionEnabled = true
    v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapDimmedView)))
    return v
  }()

  public weak var delegate: ContentViewDelegate?

  public var subscriptions = Set<AnyCancellable>()

  open override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  open override func viewDidLoad() {
    super.viewDidLoad()

    view.addSubview(dimmedView)
    dimmedView.frame = view.bounds

    setupContentView()
    setupGesture()
    observeStore()

  }

  ///set not dismissable
  public func setDismissable(_ value: Bool) -> BottomSheetContentController {
    isDismissable = value
    return self
  }

  ///set fixed height
  public func setUsedFixedHeight(with height: CGFloat) -> BottomSheetContentController {
    useFixedHeight = true
    fixedHeight = height
    return self
  }
  
  ///set store
  @available(*, deprecated, message: "don't use this method")
  public func setStore(_ store: SheetStore.Type) -> BottomSheetContentController{
    self.store = store.init()
    return self
  }
  
  ///set store
  public func setStore(_ store: SheetStore) -> BottomSheetContentController{
    self.store = store
    return self
  }
  
  ///get store
  public func getStore() -> SheetStore {
    return store
  }
  
  ///set content view
  public func setContentView(_ contentView: BottomSheetContentView.Type) -> BottomSheetContentController{
    self.contentView = contentView.init(store: store)
    return self
  }
  
  ///set content view
  public func setContentView(_ contentView: BottomSheetContentView) -> BottomSheetContentController{
    self.contentView = contentView
    return self
  }
  
  //MARK: - Constraint
  open func setupContentView() {
    self.view.addSubview(contentView)
    heightConstraint = contentView.heightAnchor.constraint(equalToConstant: 0)
    bottomConstraint = contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    NSLayoutConstraint.activate([
      contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      bottomConstraint,
      heightConstraint
    ])
  }
  
  public func setupBottomSheet(toParent: UIViewController) {
    didMove(toParent: toParent)
    toParent.addChild(self)
    toParent.view.addSubview(view)
    
    NSLayoutConstraint.activate([
      view.topAnchor.constraint(equalTo: view.topAnchor),
      view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
  }
  
  private func setupGesture() {
    let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan(recognizer:)))
    contentView.handleArea.addGestureRecognizer(panGestureRecognizer)
  }
  
  @objc
  func handleCardPan(recognizer: UIPanGestureRecognizer) {
    let translation = recognizer.translation(in: view)
    newHeight = currentContainerHeight - translation.y
    
    switch recognizer.state {
    case .changed:
      heightConstraint.constant = newHeight
      view.layoutIfNeeded()
    case .ended:
      if newHeight < defaultCardHeight || newHeight > defaultCardHeight {
        animateCardHeight(defaultCardHeight)
      }
      
      if newHeight < minimumCardHeight && isDismissable {
        animateCardDismiss()
      }
    default:
      break
    }
    
  }

  @objc
  func didTapDimmedView() {
    if isDismissable {
      animateCardDismiss()
    }
  }

  
  public func removeSheet() {
    willMove(toParent: nil)
    view.removeFromSuperview()
    removeFromParent()
    contentView = nil
    store = nil
  }
  
  open func observeStore() {
    
    if useFixedHeight {
      animatePresentCard(height: fixedHeight, delay: 0.5)
      return
    }
    
    actualHeight
      .receive(on: DispatchQueue.main)
      .sink { [weak self] height in
        self?.animatePresentCard(height: height, delay: 0.2)
      }
      .store(in: &subscriptions)
  }
  
  deinit {
    print("deinit", String(describing: BottomSheetContentController.self))
  }
  
}

extension BottomSheetContentController {
  
  // MARK: Present and dismiss animation
  public func animatePresentCard(with value: CGFloat) {
    UIView.animate(withDuration: 0.3, delay: 0.2) {
      self.dimmedView.alpha = 0.5
      self.contentView.layer.cornerRadius = 8
      self.defaultCardHeight = min(screen.height - 80, value + 16 + 16 + 16 + 80)
      self.currentContainerHeight = self.defaultCardHeight
      self.heightConstraint.constant = self.defaultCardHeight
      self.view.layoutIfNeeded()
    }
  }
  
  public func animatePresentCard(height: CGFloat, delay: CGFloat) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
      UIView.animate(
        withDuration: 0.3,
        delay: 0,
        usingSpringWithDamping: 0.6,
        initialSpringVelocity: 1
      ) {

        self.dimmedView.alpha = 0.6
        self.contentView.layer.cornerRadius = 8
        self.defaultCardHeight = min(screen.height - 80, height + 16 + 16 + 16 + 80)
        self.currentContainerHeight = self.defaultCardHeight
        self.heightConstraint.constant = self.defaultCardHeight
        self.view.layoutIfNeeded()
      }

    }
    
  }
  
  func animateCardHeight(_ height: CGFloat) {
    UIView.animate(withDuration: 0.4) { [weak self] in
      self?.heightConstraint.constant = height
      self?.view.layoutIfNeeded()
    }
    
    currentContainerHeight = height
  }
  
  func animateCardDismiss() {
    UIView.animate(withDuration: 0.4) { [weak self] in
      self?.dimmedView.alpha = 0
      self?.heightConstraint?.constant = 0
      self?.view.layoutIfNeeded()
    } completion: { [weak self] _ in
      self?.removeSheet()
      self?.delegate?.dismissed()
    }
  }
  
}
