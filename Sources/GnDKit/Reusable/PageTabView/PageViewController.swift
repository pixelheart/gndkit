//
//  PageViewController.swift
//  Testing
//
//  Created by Ilham Prabawa on 09/04/23.
//

import UIKit
import Combine

public class PageViewController: UIPageViewController {
  
  public var controllers: [UIViewController] = [] {
    didSet {
      setViewControllers(
        [controllers[0]],
        direction: .forward,
        animated: true
      )
    }
  }
  
  public var slideTo = CurrentValueSubject<IndexPath, Never>(IndexPath(item: 0, section: 0))

  public override init(transitionStyle style: UIPageViewController.TransitionStyle,
                       navigationOrientation: UIPageViewController.NavigationOrientation,
                       options: [UIPageViewController.OptionsKey : Any]? = nil) {
    
    super.init(transitionStyle: style, navigationOrientation: navigationOrientation)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  public override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    delegate = self
    dataSource = self
  }
  
}

extension PageViewController: UIPageViewControllerDataSource {
  
  public func pageViewController(
    _ pageViewController: UIPageViewController,
    viewControllerBefore viewController: UIViewController
  ) -> UIViewController? {
    
    guard let indexOfCurrentPageViewController = controllers.firstIndex(of: viewController) else {
      return nil
    }
    
    if indexOfCurrentPageViewController == 0 {
      return nil
    }
    
    return controllers[indexOfCurrentPageViewController - 1]
  }
  
  public func pageViewController(
    _ pageViewController: UIPageViewController,
    viewControllerAfter viewController: UIViewController
  ) -> UIViewController? {
    
    guard let indexOfCurrentPageViewController = controllers.firstIndex(of: viewController)
    else {
      return nil
    }
    
    if indexOfCurrentPageViewController == controllers.count - 1 {
      return nil
    }
    
    return controllers[indexOfCurrentPageViewController + 1]
    
  }
  
}

extension PageViewController: UIPageViewControllerDelegate {
  
  public func pageViewController(
    _ pageViewController: UIPageViewController,
    willTransitionTo pendingViewControllers: [UIViewController]
  ) {
   
  }
  
  public func pageViewController(
    _ pageViewController: UIPageViewController,
    didFinishAnimating finished: Bool,
    previousViewControllers: [UIViewController],
    transitionCompleted completed: Bool
  ) {
    
    guard completed else { return }

    guard let currentVC = pageViewController.viewControllers?.first else { return }

    guard let currentVCIndex = controllers.firstIndex(of: currentVC) else { return }

    let indexPathCollectionView = IndexPath(item: currentVCIndex, section: 0)

    slideTo.send(indexPathCollectionView)
  
  }
  
}
