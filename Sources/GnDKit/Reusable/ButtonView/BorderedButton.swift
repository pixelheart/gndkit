//
//  BorderdButton.swift
//  
//
//  Created by Ilham Prabawa on 04/06/23.
//

import SwiftUI

public struct BorderedButton: View {
  
  let title: String
  let action: () -> Void
  let textColor: Color
  let strokeColor: Color
  let radius: CGFloat

  public init(
    radius: CGFloat = 8,
    title: String,
    textColor: Color = .white,
    strokeColor: Color = .white,
    action: @escaping () -> Void
  ) {
    self.radius = radius
    self.title = title
    self.textColor = textColor
    self.strokeColor = strokeColor
    self.action = action
  }
  
  public var body: some View {
    Button {
      action()
    } label: {
      Text(title)
        .foregroundColor(textColor)
        .padding(.horizontal, 10)
        .padding(.vertical, 5)
        .overlay(
          RoundedRectangle(cornerRadius: radius)
            .stroke(strokeColor, lineWidth: 1)
        )
    }
    
  }
  
}

struct BorderdButton_Previews: PreviewProvider {
  static var previews: some View {
    BorderedButton(radius: 10, title: "Edit >", action: {})
  }
}
