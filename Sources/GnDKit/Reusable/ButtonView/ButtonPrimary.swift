//
//  ButtonPrimary.swift
//  
//
//  Created by Ilham Prabawa on 09/06/23.
//

import SwiftUI

public struct ButtonPrimary: View {
  
  private let title: String
  private let color: Color
  private let action: () -> Void
  private var width: CGFloat?
  private var height: CGFloat?
  
  public init(
    title: String,
    color: Color = .gray,
    action: @escaping () -> Void
  ) {
    
    self.title = title
    self.color = color
    self.action = action
  }
  
  public init(
    title: String,
    color: Color = .clear,
    width: CGFloat? = 50,
    height: CGFloat? = 50,
    action: @escaping () -> Void
  ) {
    
    self.init(
      title: title,
      color: color,
      action: action
    )
    self.width = width
    self.height = height
  }
  
  public var body: some View {
    Button {
      action()
    } label: {
      Text(title)
        .font(Font(UIFont.dmSansFont(style: .title(size: 14))))
        .foregroundColor(Color.white)
        .frame(
          maxWidth: width,
          minHeight: height
        )
        .padding(.horizontal, 5)
    }
    
    .background(
      RoundedRectangle(cornerRadius: 8).fill(color)
    )
    
  }
}


struct ButtonPrimary_Previews: PreviewProvider {
  static var previews: some View {
    ButtonPrimary(
      title: "Terima",
      color: Color.buttonActiveColor,
      action: {}
    )
    
    ButtonPrimary(
      title: "Login",
      color: .buttonActiveColor,
      width: 0,
      height: 40,
      action: {}
    )
  }
}
