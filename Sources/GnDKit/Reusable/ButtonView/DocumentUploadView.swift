//
//  SwiftUIView.swift
//  
//
//  Created by Ilham Prabawa on 14/06/23.
//

import SwiftUI

public struct DocumentUploadView: View {
  
  let title: String
  
  public init(title: String) {
    self.title = title
  }
  
  public var body: some View {
    HStack(spacing: 16) {
      Image(systemName: "doc.fill")
        .foregroundColor(.buttonActiveColor)
        .padding(.leading, 16)
      Text(title)
        .foregroundColor(.buttonActiveColor)
        .bodyStyle(size: 18)
      Spacer()
      Image("navigate_arrow", bundle: .module)
        .renderingMode(.template)
        .foregroundColor(.buttonActiveColor)
        .padding(.trailing, 16)
    }
    .frame(height: 50)
    .background(Color.buttonActiveColor.opacity(0.2))
    .cornerRadius(12)
  }
}

struct DocumentUploadView_Previews: PreviewProvider {
  static var previews: some View {
    DocumentUploadView(title: "KTP")
  }
}
