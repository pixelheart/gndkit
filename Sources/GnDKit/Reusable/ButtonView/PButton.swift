//
//  PButton.swift
//  
//
//  Created by Ilham Prabawa on 02/05/23.
//

import SwiftUI

public struct PButton: View {
  
  let title: String
  var didTap: () -> Void
  @Binding var enabled: Bool
  
  public init(title: String,
              enabled: Binding<Bool> = .constant(false),
              didTap: @escaping () -> Void) {
    
    self.title = title
    self._enabled = enabled
    self.didTap = didTap
  }
  
  public var body: some View {
    Button {
      didTap()
    } label: {
      Text(title)
        .font(Font(UIFont.dmSansFont(style: .title(size: 14))))
        .frame(maxWidth: .infinity, maxHeight: 40)
        .foregroundColor(.white)
    }
    .background(enabled ? Color.buttonActiveColor : Color.gray.opacity(0.4))
    .clipShape(RoundedRectangle(cornerRadius: 8))
    .disabled(!enabled)
    .animation(.default, value: enabled)
  }
}

struct PButton_Previews: PreviewProvider {
  static var previews: some View {
    PButton(title: "Register",
            enabled: .constant(false),
            didTap: {})
  }
}
