//
//  BButton.swift
//  
//
//  Created by Ilham Prabawa on 19/04/23.
//

import UIKit

public class BButton: UIButton {
  
  private let indicatorView: UIActivityIndicatorView = {
    let iv = UIActivityIndicatorView(style: .medium)
    iv.translatesAutoresizingMaskIntoConstraints = false
    return iv
  }()

  public var title: String = "" {
    didSet {
      setLabel()
    }
  }
  
  public var color: UIColor = UIColor.blue {
    didSet {
      self.backgroundColor = color
    }
  }
  
  public var disabledColor: UIColor? = nil
  
  public var isLoading: Bool = false {
    didSet {
      isLoading ? indicateLoading() : indicateNotLoading()
    }
  }
  
  public var didTap: (() -> Void)?
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    
    layer.cornerRadius = 8
    setAction()
    
  }
  
  public required init?(coder: NSCoder) {
    super.init(coder: coder)
    
    layer.cornerRadius = 8
    setAction()
  }
  
  public func indicateLoading() {
    UIView.animate(withDuration: 1, delay: 0) {
      self.backgroundColor = self.disabledColor
      self.isEnabled = false
      
      let attributedButton = NSAttributedString(
        string: "",
        attributes: [
          NSAttributedString.Key.foregroundColor: UIColor.white,
        ]
      )
      
      self.setAttributedTitle(attributedButton, for: .normal)
    } completion: { _ in
      self.addIndicatorView()
      self.layoutIfNeeded()
    }
  }
  
  public func indicateNotLoading() {
    UIView.animate(withDuration: 1, delay: 0) {
      self.setLabel()
      self.isEnabled = true
      self.backgroundColor = self.color
      self.removeIndicatorView()
    }
  }
  
  private func addIndicatorView() {
    addSubview(indicatorView)
    NSLayoutConstraint.activate([
      indicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      indicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
    ])
    indicatorView.startAnimating()
  }
  
  private func removeIndicatorView() {
    indicatorView.stopAnimating()
    indicatorView.removeFromSuperview()
    
  }
  
  private func setAction() {
    addTarget(self, action: #selector(didClick), for: .touchUpInside)
  }
  
  private func setLabel() {
    let attributedButton = NSAttributedString(
      string: title,
      attributes: [
        NSAttributedString.Key.font: UIFont.dmSansFont(style: .title(size: 14)),
        NSAttributedString.Key.foregroundColor: UIColor.white,
      ]
    )
    setAttributedTitle(attributedButton, for: .normal)
  }
  
  @objc
  private func didClick() {
    didTap?()
  }
  
}
