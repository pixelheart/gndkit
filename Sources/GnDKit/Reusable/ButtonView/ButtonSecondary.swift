//
//  ButtonTernary.swift
//
//
//  Created by Ilham Prabawa on 09/06/23.
//

import SwiftUI

public struct ButtonSecondary: View {

  private let title: String
  private let backgroundColor: Color
  private let tintColor: Color
  private let action: () -> Void
  private var width: CGFloat?
  private var height: CGFloat?
  private var cornerRadius: CGFloat?

  public init(
    title: String,
    backgroundColor: Color = .gray,
    tintColor: Color = .gray,
    action: @escaping () -> Void
  ) {

    self.title = title
    self.backgroundColor = backgroundColor
    self.tintColor = tintColor
    self.action = action
  }

  public init(
    title: String,
    backgroundColor: Color = .gray,
    tintColor: Color = .gray,
    width: CGFloat? = 50,
    height: CGFloat? = 50,
    action: @escaping () -> Void
  ) {

    self.init(
      title: title,
      backgroundColor: backgroundColor,
      tintColor: tintColor,
      action: action
    )

    self.width = width
    self.height = height
  }

  public init(
    title: String,
    backgroundColor: Color = .gray,
    tintColor: Color = .gray,
    cornerRadius: CGFloat,
    width: CGFloat = 50,
    height: CGFloat = 50,
    action: @escaping() -> Void
  ) {

    self.init(
      title: title,
      backgroundColor: backgroundColor,
      tintColor: tintColor,
      width: width,
      height: height,
      action: action
    )

    self.cornerRadius = cornerRadius
  }

  public var body: some View {
    Button {
      action()
    } label: {
      Text(title)
        .font(Font(UIFont.dmSansFont(style: .title(size: 14))))
        .foregroundColor(tintColor)
        .frame(
          maxWidth: width,
          minHeight: height
        )
        .padding(.horizontal, 5)
    }
    .background(backgroundColor)
    .clipShape(RoundedRectangle(cornerRadius: cornerRadius ?? 8))
    .overlay(
      RoundedRectangle(cornerRadius: cornerRadius ?? 8)
        .stroke(
          tintColor,
          lineWidth: 1
        )
    )
  }
}

struct ButtonTernary_Previews: PreviewProvider {
  static var previews: some View {
    ButtonSecondary(
      title: "Tolak",
      backgroundColor: Color.buttonActiveColor,
      action: {}
    )
  }
}
