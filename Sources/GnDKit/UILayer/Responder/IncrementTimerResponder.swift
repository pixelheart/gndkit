//
//  IncrementTimerResponder.swift
//
//
//  Created by Ilham Prabawa on 17/11/23.
//

import Foundation

public protocol IncrementTimerResponder {
  func start()
  func stop()
}
