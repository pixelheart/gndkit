//
//  NavigatonAction.swift
//  
//
//  Created by Ilham Prabawa on 30/03/23.
//

import Foundation

public enum NavigationAction<ViewModelType>: Equatable where ViewModelType: Equatable {
  case present(view: ViewModelType)
  case presented(view: ViewModelType)
}
