//
//  Paramable.swift
//
//
//  Created by Ilham Prabawa on 30/09/24.
//

import Foundation

public protocol Paramable {
  func toParam() -> [String: Any]
}
