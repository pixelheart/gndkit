//
//  DMSans.swift
//  
//
//  Created by Ilham Prabawa on 15/04/23.
//

import UIKit

public struct DMSans {
  
  public static func registerFonts() {
    let fontNames = ["DMSans-Bold", "DMSans-Medium", "DMSans-Regular"]
    fontNames.forEach {
      registerFont(bundle: .module, fontName: $0, fontExtension: ".ttf")
    }
  }
  
  fileprivate static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) {
    
    guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension),
          let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
          let font = CGFont(fontDataProvider) else {
      fatalError("Couldn't create font from data")
    }
    
    var error: Unmanaged<CFError>?
    
    CTFontManagerRegisterGraphicsFont(font, &error)
  }
  
}
