//
//  Transformable.swift
//
//
//  Created by Ilham Prabawa on 30/09/24.
//

import Foundation

public protocol Transformable {
  associatedtype D
  associatedtype E
  associatedtype VM

  static func map(from data: D) -> E
  static func mapTo(_ entity: E) -> VM
}
