//
//  GLog.swift
//  
//
//  Created by Ilham Prabawa on 19/09/23.
//

import Foundation

public func GLog(_ title: String, _ message: Any) {
  print("🎯 \(title) : \(message)")
}
