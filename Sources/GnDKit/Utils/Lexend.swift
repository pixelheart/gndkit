//
//  Lexend.swift
//
//
//  Created by Ilham Prabawa on 22/10/24.
//

import UIKit

public struct Lexend {

  public static func registerFonts() {
    let fontNames = ["Lexend-SemiBold", "Lexend-Regular", "Lexend-Light"]
    fontNames.forEach {
      registerFont(
        bundle: .module,
        fontName: $0,
        fontExtension: ".ttf"
      )
    }
  }

  fileprivate static func registerFont(
    bundle: Bundle,
    fontName: String,
    fontExtension: String
  ) {

    guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension),
          let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
          let font = CGFont(fontDataProvider) else {
      fatalError("Couldn't create font from data")
    }

    var error: Unmanaged<CFError>?

    CTFontManagerRegisterGraphicsFont(font, &error)
  }

}
