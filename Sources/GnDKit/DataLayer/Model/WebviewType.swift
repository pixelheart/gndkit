//
//  WebviewType.swift
//
//
//  Created by Ilham Prabawa on 14/09/23.
//

public enum WebviewType {
  case faq
  case tnc
  case privacy
  
  public func getTitleValue() -> String {
    switch self {
    case .faq:
      return "FAQ"
    case .tnc:
      return "Syarat dan Ketentuan"
    case .privacy:
      return "Kebijakan Privasi"
    }
  }
  
  public func getUrlValue() -> String {
    switch self {
    case .faq:
      return "https://perqara.com/faq?mobile=true"
    case .tnc:
      return "https://perqara.com/syarat-ketentuan?mobile=true"
    case .privacy:
      return "https://perqara.com/kebijakan-privasi?mobile=true"
    }
  }
}
