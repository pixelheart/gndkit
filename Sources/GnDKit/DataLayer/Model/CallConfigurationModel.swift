//
//  CallConfigResponse.swift
//  
//
//  Created by Ilham Prabawa on 19/09/23.
//

import Foundation

public struct CallConfigurationModel {
  public var callID: Int
  public var channel: String
  public var token: String

  public init() {
    callID = 0
    channel = ""
    token = ""
  }

  public init(
    callID: Int,
    channel: String,
    token: String
  ) {
    self.callID = callID
    self.channel = channel
    self.token = token
  }

}
