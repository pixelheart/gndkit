//
//  NetworkErrorMessage.swift
//  Ilham Hadi Prabawa
//
//  Created by Ilham Hadi Prabawa on 18/11/21.
//  Copyright © Xamben. All rights reserved.
//

// Error Code
//-------------
// -1: Failure
// -2: Can't Define error
// -3: Data Not Found
// -4: Parse Error
// -5: Uknown Error
// -6: Unauthorized
// -7: Internal Server Error

public protocol NetworkError: Error {
  var code: Int { get set }
  var description: String { get set }
}

public struct NetworkErrorMessage: NetworkError {
  
  public var code: Int
  public var description: String
  
  public init(code: Int, description: String) {
    self.code = code
    self.description = description
  }
  
}

public struct NetworkErrorWithPayload: NetworkError {
  public var code: Int
  public var description: String
  public let payload: [String: Any]
  
  public init(
    code: Int,
    description: String,
    payload: [String : Any] = [:]
  ) {
    self.code = code
    self.description = description
    self.payload = payload
  }
}
