//
//  ErrorMessage.swift
//
//
//  Created by Ilham Prabawa on 30/03/23.
//

// Error Code
//-------------
// -1: Failure
// -2: Cant Define error
// -3: Data Not Found
// -4: Parse Error
// -5: Uknown Error
// -6: Unauthorized
// -7: File Not Found

import Foundation

public struct ErrorMessage: Error {

  // MARK: - Properties
  public let id: Int
  public let title: String
  public let message: String
  public let payload: [String : Any]

  // MARK: - Constructors
  
  public init() {
    self.id = -1
    self.title = ""
    self.message = ""
    self.payload = [:]
  }
  
  public init(
    title: String,
    message: String
  ) {
    self.id = -1
    self.title = title
    self.message = message
    self.payload = [:]
  }

  public init(
    id: Int,
    title: String,
    message: String
  ) {
    self.id = id
    self.title = title
    self.message = message
    self.payload = [:]
  }

  public init(
    id: Int,
    title: String,
    message: String,
    payload: [String : Any]
  ) {
    self.id = id
    self.title = title
    self.message = message
    self.payload = payload
  }

}

extension ErrorMessage: Equatable {

  public static func ==(lhs: ErrorMessage, rhs: ErrorMessage) -> Bool {
    return lhs.id == rhs.id
  }
}
