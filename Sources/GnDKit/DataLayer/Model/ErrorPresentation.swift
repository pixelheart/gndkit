//
//  ErrorPresentation.swift
//  
//
//  Created by Ilham Prabawa on 30/03/23.
//

import Foundation


public enum ErrorPresentation {
  
  case presenting
  case dismissed
}

