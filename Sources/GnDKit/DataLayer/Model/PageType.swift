//
//  PageType.swift
//  
//
//  Created by Ilham Prabawa on 14/09/23.
//

import Foundation

public enum PageType: Int {
  case chat = 0
  case summary = 1
  case incoming = 2
}
