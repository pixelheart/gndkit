//
//  ChangePasswordType.swift
//  
//
//  Created by Ilham Prabawa on 14/09/23.
//

public struct ChangePasswordType {
  let type: String
  let input: String

  public init() {
    self.type = ""
    self.input = ""
  }

  public init(type: String, input: String) {
    self.type = type
    self.input = input
  }

}
