//
//  Gender.swift
//  
//
//  Created by Ilham Prabawa on 14/06/23.
//

public enum Gender {
  case men
  case women
}
