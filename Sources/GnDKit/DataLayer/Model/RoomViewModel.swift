//
//  RoomViewModel.swift
//  
//
//  Created by Ilham Prabawa on 07/08/23.
//

import Foundation

public struct RoomViewModel {
  public let roomKey: String
  public let consultationID: Int
  public let lawyerID: Int
  public let clientID: Int
  public let clientName: String
  public let imageURL: URL?
  public let timeRemaining: TimeInterval
  
  public init() {
    self.roomKey = ""
    self.consultationID = 0
    self.lawyerID = 0
    self.clientID = 0
    self.clientName = ""
    self.imageURL = nil
    self.timeRemaining = 0.0
  }
  
  public init(roomKey: String,
              consultationID: Int,
              lawyerID: Int,
              clientID: Int,
              clientName: String,
              imageURL: URL?,
              timeRemaining: TimeInterval) {
    
    self.roomKey = roomKey
    self.consultationID = consultationID
    self.lawyerID = lawyerID
    self.clientID = clientID
    self.clientName = clientName
    self.imageURL = imageURL
    self.timeRemaining = timeRemaining
  }
  
}
