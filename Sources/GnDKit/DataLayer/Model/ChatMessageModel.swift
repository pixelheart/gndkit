//
//  ChatMessageViewModel.swift
//
//
//  Created by Ilham Prabawa on 17/08/23.
//

import Foundation

public enum FileType {
  case image
  case document
  case unknown
}

public enum SenderType: CaseIterable {
  case lawyer
  case client
  case system
  case unknown
}

public enum NotifyType: CaseIterable {
  case voiceCallEnded
  case videoCallEnded
  case missedVoiceCall
  case missedVideoCall
  case unknown
}

extension NotifyType {
  public static func mapType(_ str: String) -> Self {
    switch str {
    case "VOICE_CALL_ENDED":
      return .voiceCallEnded
    case "MISSED_VOICE_CALL":
      return .missedVoiceCall
    case "MISSED_VIDEO_CALL":
      return .missedVideoCall
    case "VIDEO_CALL_ENDED":
      return .videoCallEnded
    default:
      return .unknown
    }
  }
}

extension SenderType {
  public static func mapSender(_ str: String) -> Self {
    switch str {
    case "LAWYER":
      return .lawyer
    case "CLIENT":
      return .client
    case "SYSTEM":
      return .system
    default:
      return .unknown
    }
  }
}

public struct ChatMessageModel {
  public var id: String
  public var message: String
  public var isIncoming: Bool
  public var senderType: SenderType
  public var notifyType: NotifyType
  public var sendAt: String
  public var file: FileModel?
  public var referenceID: String?
  public var tapAction: ((ChatMessageModel) -> Void)?

  public init() {
    self.message = ""
    self.isIncoming = false
    self.sendAt = ""
    self.notifyType = .unknown
    self.file = nil
    self.senderType = .client
    self.tapAction = { _ in }
    self.id = ""
    self.referenceID = ""
  }

  public init(
    message: String,
    isIncoming: Bool,
    senderType: SenderType,
    notifyType: NotifyType,
    sendAt: String,
    file: FileModel? = nil,
    tapAction: ((ChatMessageModel) -> Void)? = nil
  ) {
    self.message = message
    self.isIncoming = isIncoming
    self.senderType = senderType
    self.notifyType = notifyType
    self.sendAt = sendAt
    self.file = file
    self.tapAction = tapAction
    self.id = ""
    self.referenceID = ""
  }

  public init(
    id: String,
    message: String,
    isIncoming: Bool,
    senderType: SenderType,
    notifyType: NotifyType,
    sendAt: String,
    file: FileModel? = nil,
    referenceID: String?,
    tapAction: ((ChatMessageModel) -> Void)? = nil
  ) {
    self.id = id
    self.message = message
    self.isIncoming = isIncoming
    self.senderType = senderType
    self.notifyType = notifyType
    self.sendAt = sendAt
    self.file = file
    self.tapAction = tapAction
    self.referenceID = referenceID
  }

  public struct FileModel {
    public var url: URL?
    public var size: Int
    public var name: String
    public var ext: String

    public init() {
      self.url = nil
      self.size = 0
      self.name = ""
      self.ext = ""
    }

    public init(url: URL?,
                size: Int,
                name: String,
                ext: String) {

      self.url = url
      self.size = size
      self.name = name
      self.ext = ext
    }


    public func getFileType() -> FileType {

      let imageExt = [".jpg, .jpeg, .png", ".heic"]

      for img in imageExt {

        if img.contains(ext) {
          return .image
        }

      }

      return .document
    }

  }


}
