//
//  LoginType.swift
//  
//
//  Created by Ilham Prabawa on 14/09/23.
//

public struct LoginType {
  public let type: String
  public let input: String

  public init() {
    self.type = ""
    self.input = ""
  }

  public init(type: String, input: String) {
    self.type = type
    self.input = input
  }

}
