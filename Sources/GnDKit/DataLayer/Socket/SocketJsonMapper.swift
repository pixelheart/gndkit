//
//  SocketJsonMapper.swift
//  
//
//  Created by Ilham Prabawa on 29/08/23.
//

import Foundation

public struct SocketJsonMapper {

  public init() {}

  public static func parseEndConsultation(with data: [Any]) -> (Int, Int, String) {
    var consultationID = 0
    var clientID = 0
    var roomKey = ""

    let data = try? JSONSerialization.data(withJSONObject: data.first!)
    let jsonObject = try? JSONSerialization.jsonObject(with: data!, options: .fragmentsAllowed)
    
    if let data = jsonObject as? [String: Any] {
      if let booking = data["booking"] as? [String: Any] {
        if let consultID = booking["consultation_id"] as? Int {
          consultationID = consultID
        }
      }
      if let client = data["client"] as? [String: Any] {
        if let id = client["id"] as? Int {
          clientID = id
        }
      }
      if let room_key = data["room_key"] as? String {
        roomKey = room_key
      }
    }

    return (consultationID, clientID, roomKey)
  }

  public static func parsingReceivedMessage(from data: Any) throws -> ChatMessageModel? {
    do {
      let jsonData = try JSONSerialization.data(
        withJSONObject: data,
        options: .fragmentsAllowed
      )

      var model: ChatMessageModel = .init()
      var file = ChatMessageModel.FileModel()

      let jsonObject = try JSONSerialization.jsonObject(with: jsonData)

      if let json = jsonObject as? [String: Any] {

        if let id = json["_id"] as? String {
          model.id = id
        }

        if let referenceID = json["reference_id"] as? String {
          model.referenceID = referenceID
        }

        if let consultID = json["consultation_id"] as? Int{
          GLog("consultation_id_int", consultID)
        }

        if let consultID = json["consultation_id"] as? String {
          GLog("consultation_id_string", consultID)
        }

        if let message = json["message"] as? String {
          model.message = message
        }

        if let sendAt = json["sent_at"] as? Double {
          model.sendAt = sendAt.convertFromEpoch(with: "HH:mm")
        }

        if let sender = json["sender"] as? String  {
          model.isIncoming = sender != "LAWYER"
          model.senderType = SenderType.mapSender(sender)
        }

        if let notify = json["notify"] as? [String: Any] {
          if let type = notify["notify_type"] as? String {
            model.notifyType = NotifyType.mapType(type)
          }
        }

        if let jsonFile = json["file"] as? [String: Any] {

          if let url = jsonFile["url"] as? String {
            file.url = URL(string: url)
          }

          if let fileSize = jsonFile["file_size"] as? Int {
            file.size = fileSize
          }

          if let fileName = jsonFile["file_name"] as? String {
            file.name = fileName
          }

          if let fileExt = jsonFile["file_ext"] as? String {
            file.ext = fileExt
          }

          model.file = file

        }

      }

      return model

    }catch {
      throw ErrorMessage(
        id: -1,
        title: "Parse Error",
        message: error.localizedDescription
      )
    }

  }

  public static func parsingClientTyping(from data: Any) -> Bool {
    do {
      let jsonData = try JSONSerialization.data(
        withJSONObject: data,
        options: .fragmentsAllowed
      )

      let jsonObject = try JSONSerialization.jsonObject(with: jsonData)

      if let json = jsonObject as? [String: Any] {
        if let onTyping = json["on_typing"] as? Bool {
          return onTyping
        }
      }

    } catch {
      return false
    }

    return false
  }

}
