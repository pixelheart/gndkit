//
//  SocketServiceConsultationManager.swift
//
//
//  Created by Ilham Prabawa on 04/09/23.
//

import Foundation
import SocketIO
import Combine

public protocol SocketServiceChatDelegate: SocketReceiveable, AnyObject {
  func chatConnected()
  func listenClient(typing state: Bool)
}

public protocol SocketServiceCallDelegate: AnyObject {
  func didCallRequest(_ response: Any)
  func didMuted(_ response: Any)
  func didCancelled()
  func didCallResponse(_ response: Any)
  func didCallFailed()
}

extension SocketServiceCallDelegate {

  public func didMuted(_ response: Any) {

  }

  public func didCancelled() {

  }

  public func didCallResponse(_ response: Any) {

  }

  public func didCallFailed() {

  }
}

open class SocketServiceConsultationManager: SocketServiceChat {

  //MARK: - Dependenciey
  private let socketURL: String

  //MARK: - computed property
  public var socket: SocketIOClient!
  public var socketManager: SocketManager!
  public weak var delegate: SocketServiceChatDelegate?
  public weak var callDelegate: SocketServiceCallDelegate?

  public init(socketURL: String) {
    self.socketURL = socketURL
  }

  open func start(
    with roomKey: String,
    consultationID: Int,
    lawyerID: Int,
    clientID: Int
  ) {

    guard let socketURL = URL(string: socketURL)
    else { return }

    socketManager = SocketManager(
      socketURL: socketURL,
      config: [
        .connectParams([
          "EIO": "4",
          "room_key": roomKey,
          "consultation_id": consultationID,
          "client_id": clientID,
          "lawyer_id": lawyerID
        ]),
        .forcePolling(true),
        .compress,
        .log(false)
      ]
    )

    socket = socketManager.socket(forNamespace: "/consultation")

    socket.on(clientEvent: .connect) { [weak self] _, _ in
      self?.chatConnect()
      self?.delegate?.chatConnected()
      GLog("⚡️", "CHAT SOCKET CONNECTED")
    }

    socket.on(clientEvent: .disconnect) { _, _ in
      GLog("⚡️", "CHAT SOCKET DISCONNECTED")
    }

    socket.on(clientEvent: .error) { _, _ in
      GLog("⚡️", "CHAT SOCKET ERROR")
    }

    socket.on(clientEvent: .reconnect) { _, _ in
      GLog("⚡️", "CHAT SOCKET RECONNECT")
    }

    socket.on("CHAT:RECEIVED") { [weak self] data, ack in
      GLog("⚡️", "chat data: \(data[0])")
      guard let self = self else { return }
      do {
        let message = try SocketJsonMapper.parsingReceivedMessage(from: data[0])
        self.delegate?.onReceived(with: message!)
      }catch {
        if let errMessage = error as? ErrorMessage {
          GLog("Error", errMessage)
        }
      }

      ack.with("success")

    }

    socket.on("LAWYER:CALL_REQUEST_NOTIFICATION") { [weak self] data, ack in
      self?.callDelegate?.didCallRequest(data[0])
      GLog("⚡️", "CLIENT HAS A CALL REQUEST")
      ack.with("success")
    }

    socket.on("LAWYER:CALL_MUTED_NOTIFICATION") { [weak self] data, ack in
      self?.callDelegate?.didMuted(data[0])
      GLog("⚡️", "CLIENT HAS BEEN MUTED")
      ack.with("success")
    }

    socket.on("LAWYER:CALL_CANCELED_NOTIFICATION") { [weak self] data, ack in
      self?.callDelegate?.didCancelled()
      GLog("⚡️", "CLIENT HAS BEEN CANCELED \(data[0])")
      ack.with("success")
    }

    socket.on("LAWYER:CALL_RESPONSE_NOTIFICATION") { [weak self] data, ack in
      self?.callDelegate?.didCallResponse(data[0])
      GLog("⚡️", "CLIENT CALL RESPONSE \(data[0])")
      ack.with("success")
    }

    socket.on("LAWYER:CALL_FAILED_NOTIFICATION") { [weak self] data, ack in
      self?.callDelegate?.didCallFailed()
      GLog("⚡️", "CLIENT HAS BEEN FAILED \(data[0])")
      ack.with("success")
    }

    socket.on("CHAT:CLIENT_TYPING_LISTEN") { [weak self] data, ack in
      GLog("⚡️", "LISTEN CLIENT TYPING")
      let state = SocketJsonMapper.parsingClientTyping(from: data[0])
      self?.delegate?.listenClient(typing: state)
      ack.with("success")
    }

    socket.connect()
  }

  public func send(
    consultationID: Int,
    lawyerName: String,
    message: String
  ) {

    socket.emitWithAck(
      "CHAT:SEND_TEXT",
      [
        "consultation_id": consultationID,
        "user_name": lawyerName,
        "sender": "LAWYER",
        "message": message,
        "sent_at": Date().currentTimeMillis(),
        "delivered_at": Date().currentTimeMillis(),
        "read_at": Date().currentTimeMillis()
      ] as [String : Any]

    ).timingOut(after: 5) { _ in

    }

  }

  public func send(
    consultationID: Int,
    lawyerName: String,
    message: String,
    referenceID: String?
  ) {
    var requests: [String: Any] = [
      "consultation_id": consultationID,
      "user_name": lawyerName,
      "sender": "LAWYER",
      "message": message,
      "sent_at": Date().currentTimeMillis(),
      "delivered_at": Date().currentTimeMillis(),
      "read_at": Date().currentTimeMillis(),
    ]

    if let referenceID = referenceID {
      requests["reference_id"] = referenceID
    }

    socket.emitWithAck(
      "CHAT:SEND_TEXT",
      requests
    ).timingOut(after: 5) { _ in

    }
  }

  public func chatConnect() {
    socket
      .emitWithAck("CHAT:CONNECT")
      .timingOut(
        after: 3,
        callback: { _ in

        }
      )
  }

  public func stopChat() {
    guard let _ = socket,
          let _ = socketManager
    else { return }

    socket.removeAllHandlers()
    socket.disconnect()
    socketManager.disconnect()

    socketManager = nil
    socket = nil
    GLog("⚡️", "CHAT SOCKET TERMINATED")
  }

  public func emit(_ onTyping: Bool) {
    socket
      .emitWithAck(
        "CHAT:LAWYER_TYPING",
        ["on_typing": onTyping]
      )
      .timingOut(after: 5) { ack in
        GLog("⚡️", "EMIT LAWYER TYPING")
      }
  }

}
