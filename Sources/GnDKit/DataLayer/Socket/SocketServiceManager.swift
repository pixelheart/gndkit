//
//  SocketServiceManager.swift
//
//
//  Created by Ilham Prabawa on 12/07/23.
//

import Foundation
import SocketIO

public protocol SocketReceiveable {
  func onReceived(with message: ChatMessageModel)
}

public protocol SocketServiceDelegate: AnyObject {
  func onConnected()

  func onNewConsultation()

  func onJoinRoom()

  func onEndConsultation(
    consultationID: Int,
    clientID: Int,
    roomKey: String
  )

  func onLawyerMissedCall()

}

public class SocketServiceManager: SocketService {

  //MARK: - Dependenciey
  private let socketURL: String

  private var socket: SocketIOClient!
  private var socketManager: SocketManager!
  public weak var delegate: SocketServiceDelegate?

  public init(socketURL: String) {
    self.socketURL = socketURL
  }

  public var isStarted: Bool {
    get {
      guard let _ = socketManager else {
        return false
      }
      return socketManager.status.active
    }
  }

  public var isRegistered: Bool = false

  public func start() {
    guard let socketURL = URL(string: socketURL)
    else { return }

    socketManager = SocketManager(
      socketURL: socketURL,
      config: [
        .connectParams(["EIO": "4"]),
        .forcePolling(true),
        .compress,
        .log(false)
      ]
    )

    socket = socketManager.defaultSocket

    socket.on(clientEvent: .connect) { data, ack in
      GLog("⚡️", "GLOBAL SOCKET CONNECTED")
      self.delegate?.onConnected()
    }

    socket.on("LAWYER:END_CONSULTATION_NOTIFICATION") { [weak self] data, ack in
      GLog("⚡️", "END CONSULTATION")
      guard let self = self else { return }

      let (consultationID, clientID, roomKey) = SocketJsonMapper.parseEndConsultation(with: data)

      self.delegate?.onEndConsultation(
        consultationID: consultationID,
        clientID: clientID,
        roomKey: roomKey
      )

      ack.with("success")
    }

    socket.on("LAWYER:NEW_CONSULTATION") { data, ack in
      GLog("⚡️", "NEW CONSULTATION")
      self.delegate?.onNewConsultation()
      ack.with("success")
    }

    socket.on("LAWYER:JOIN_ROOM") { data, ack in
      GLog("⚡️", "LAWYER JOIN ROOM")
      self.delegate?.onJoinRoom()
      ack.with("success")
    }

    socket.on("LAWYER:REQUIRED_MISSED_CALL_REASON") { data, ack in
      GLog("⚡️", "LAWYER MISSEDCALL")
      self.delegate?.onLawyerMissedCall()
      ack.with("success")
    }

    socket.connect()
  }

  func connect() {
    if let _ = socketManager {
      socket.connect()
    }
  }

  public func stop() {
    //    isRegistered = false

    guard let _ = socket,
          let _ = socketManager
    else { return }

    socket.removeAllHandlers()
    socket.disconnect()
    socketManager.disconnect()

    socketManager = nil
    socket = nil
    GLog("⚡️", "GLOBAL SOCKET TERMINATED")
  }

  public func onEndConsultation(completion: @escaping() -> Void) {

  }

  //MARK: Emitters
  public func register(token: String, id: Int) {
    socket.emit(
      "LAWYER:REGISTER",
      [
        "token": token,
        "lawyer_id": "\(id)"
      ]
    )

    GLog("⚡️", "LAWYER REGISTERED")
  }

  @available(*, deprecated, message: "Now using API")
  public func online() {
    socket.emit("LAWYER:TRIGGER_ONLINE")
  }

  @available(*, deprecated, message: "Now using API")
  public func offline() {
    socket.emit("LAWYER:TRIGGER_OFFLINE")
  }

}
