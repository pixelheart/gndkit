//
//  SocketService.swift
//
//
//  Created by Ilham Prabawa on 12/07/23.
//

import Foundation
import Combine

public protocol SocketService {
  var isStarted: Bool { get }
  var isRegistered: Bool { get set }
  func start()
  func stop()
  func register(token: String, id: Int)
  func online()
  func offline()
  func onEndConsultation(completion: @escaping() -> Void)

}

public protocol SocketServiceChat {

  func start(
    with roomKey: String,
    consultationID: Int,
    lawyerID: Int,
    clientID: Int
  )

  @available(
    *,
     deprecated,
     renamed: "send(consultationID:lawyerName:message:referenceID:)",
     message: "use send with reference ID instead"
  )
  func send(
    consultationID: Int,
    lawyerName: String,
    message: String
  )

  func send(
    consultationID: Int,
    lawyerName: String,
    message: String,
    referenceID: String?
  )

  func chatConnect()

  func stopChat()

  func emit(_ onTyping: Bool)

}

public enum CallType: String {
  case voice = "voice"
  case video = "video"
}
