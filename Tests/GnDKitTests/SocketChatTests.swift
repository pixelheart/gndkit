//
//  SocketChatTests.swift
//  
//
//  Created by Ilham Prabawa on 27/09/23.
//

import XCTest
@testable import GnDKit

final class SocketChatTests: XCTestCase {

  var sut: SocketJsonMapper!

  override func setUp() {
    super.setUp()

    sut = SocketJsonMapper()
  }

  override func tearDown() {
    super.tearDown()
    sut = nil
  }

  func testMapReceivedMessage() {
    //given
    var model: ChatMessageModel?
    let data = """
   {
       "__v" = 0;
       "_id" = 667a7f7f313da892219b872d;
       "consultation_id" = 4126;
       "delivered_at" = 0;
       file =     {
       };
       message = "test message";
       notify =     {
       };
       "read_at" = 0;
       "reference_id" = "";
       sender = CLIENT;
       "sent_at" = 1719304062717;
       "user_name" = Dio;
   }
"""

    //when
    model = try? SocketJsonMapper.parsingReceivedMessage(from: data)

    //then
    XCTAssertNotNil(model)

  }

  func testSocketChat_whenEmitTyping_andReturnSuccess() {
    //given
    let socketChat = MockSocketServiceChat()
    let onTyping: Bool = true

    //when
    socketChat.emit(true)

    //then
    XCTAssertEqual(socketChat.typingState, onTyping)
  }

}

class MockSocketServiceChat: SocketServiceChat {

  func send(
    consultationID: Int,
    lawyerName: String,
    message: String,
    referenceID: String?
  ) {

  }
  

  var typingState: Bool = false

  func start(
    with roomKey: String,
    consultationID: Int,
    lawyerID: Int,
    clientID: Int
  ) {

  }
  
  func send(
    consultationID: Int,
    lawyerName: String,
    message: String
  ) {

  }
  
  func chatConnect() {
    
  }
  
  func stopChat() {

  }
  
  func emit(_ onTyping: Bool) {
    self.typingState = onTyping
  }

}
