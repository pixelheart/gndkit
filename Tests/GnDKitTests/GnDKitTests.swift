import XCTest
@testable import GnDKit
import os

final class GnDKitTests: XCTestCase {

  func testConvertDateToString() {
    //given
    var dateStr = ""
    let timestamp: Double = 1695778276183

    //when
    dateStr = timestamp.convertFromEpoch(with: "hh:mm")

    //then
    XCTAssertEqual(dateStr, "08:31")
  }

  func test_addDaybyOne() {
    //given
    var nextDay: String = ""
    let timestamp: String = "2023-11-07T12:59:23.000000Z"

    //when
    nextDay = timestamp.toDate()?.nextDay(by: 1)?.formatted(with: "d") ?? "-1"

    //then
    XCTAssertEqual(nextDay, "8")
  }

  func test_convertStringToDate() {
    //given
    var date: Date?
    let dateStr: String = "2023-11-07T12:59:23.000000Z"

    //when
    date = dateStr.toDate()

    //then
    XCTAssertNotNil(date)
    XCTAssertEqual(date?.stringFormat(), "Selasa, November 7, 2022 19:59")

  }

  func test_calculateToNextDayReturnError() {
    //given
    var date: Date?
    let dateStr: String = "2023-11-07T12:59:23.000000Z"

    //when
    date = dateStr.toDate()?.nextDay(by: 1)

    //then
    XCTAssertNotEqual(date?.stringFormat(), "Sabtu, Januari 8, 2023 19:59")
  }

  func test_calculateToNextDay() {
    //given
    var date: Date?
    let dateStr: String = "2023-11-07T12:59:23.000000Z"

    //when
    date = dateStr.toDate()?.nextDay(by: 1)

    //then
    XCTAssertEqual(date?.stringFormat(), "Rabu, November 8, 2023 19:59")
  }

  func test_LoggerNetworking() {
    NLog(
      .error, 
      layer: "Networking",
      message: "getting data from home api"
    )

    NLog(
      .info,
      layer: "Networking",
      message: "start requesting home data"
    )

    NLog(
      .error,
      layer: "Presentation",
      message: "presenting data into tableview"
    )

    NLog(
      .critical,
      layer: "Networking",
      message: "dirty requesting home API"
    )
  }

  func test_convertTimeInterval_toDate() {
    let time: TimeInterval = 1728634513
    print("result", time.epochToDate())
  }

}
