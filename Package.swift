// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "GnDKit",
  defaultLocalization: "id",
  platforms: [.iOS(.v15)],
  products: [
    // Products define the executables and libraries a package produces, and make them visible to other packages.
    .library(
      name: "GnDKit",
      targets: ["GnDKit"]),
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    .package(
      url: "https://github.com/Alamofire/Alamofire.git",
      .upToNextMajor(from: "5.6.1")
    ),
    .package(
      url: "https://github.com/onevcat/Kingfisher.git",
      .upToNextMajor(from: "7.0.0")
    ),
    .package(
      url: "https://github.com/SnapKit/SnapKit",
      .upToNextMajor(from: "5.6.0")
    ),
    .package(
      url: "https://github.com/JonasGessner/JGProgressHUD.git",
      exact: "2.2.0"
    ),
    .package(
      url: "https://github.com/socketio/socket.io-client-swift",
      branch: "master"
    )
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages this package depends on.
    .target(
      name: "GnDKit",
      dependencies: [
        "Alamofire",
        "JGProgressHUD",
        .product(name: "Kingfisher", package: "Kingfisher"),
        .product(name: "SnapKit", package: "SnapKit"),
        .product(name: "SocketIO", package: "socket.io-client-swift"),
      ],
      resources: [
        .process("Fonts"),
        .process("Assets")
      ]
    ),
    .testTarget(
      name: "GnDKitTests",
      dependencies: ["GnDKit"]),
  ]
)
